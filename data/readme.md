# Data files

## country-concept-similarity

Cosine similarity between the embedding vectors of concepts (such as
_terrorism_, _trade_, _palm_) and country name.  Embeddings are the
300-D common crawl 840G token vectors, scraped ~2015.  Country names
are from ggplot's world map.  As the embeddings do not handle
multi-word country names, these are left missing.


## Growth-unemployment

World Bank data about GDP growth and unemployment across countries.
266 entries.

Columns:

* **country**: 3-letter acronym
* **growth**: GDP growth, pct
* **unemployemnt**: unemployment rate (pct)


## harden.csv: James Harden 2021-2022 results

Harden is an NBA player, 196 cm, 99 kg, born 1989.

2021-2022 season data from
https://www.basketball-reference.com/players/h/hardeja01/gamelog/2022

* **Rk**: rank (game id)
* **G**: season game for Harden
* **Date**: yyyy-mm-dd
* **Age**: years-days
* **Tm**: team
* **Opp**: opponent
* **GS**: games started (=1 as each row is one game)
* **MP**: minutes played
* **FG**: field goals
* **FGA**: field goal attempts
* **FG%**: field goal percentage scored
* **3P**: 3-point field goals 
* **3PA**: 3-point attempts
* **3P%**: 
* **FT**: free throw scores
* **FTA**: free throw attempts
* **FT%**:
* **ORB**: offensive rebounds
* **DRB**: defensive rebounds
* **TRB**: total rebounds
* **AST**: assists
* **STL**: steals
* **BLK**: blocks
* **TOV**: turnovers
* **PF**: personal fouls
* **PTS**: points
* **GmSc**: game score
* **+/-**: 

## howell-height-weight.csv

Downloaded from from [rethinking
package](https://github.com/rmcelreath/rethinking/blob/master/data/Howell2.csv),
but originates from University of Toronto [datasets
collection](https://tspace.library.utoronto.ca/handle/1807/10395).
The data is collected by Nancy Howell in 1960s, it
contains a number of different samples, e.g. adults only, adult men
only, kids only, some snowball sampling...  So it is not a
representative dataset.

* **age.at.death** 
* **age**
* **alive** 1/0
* **male** 1/0
* **height** cm
* **weight** kg


## marathon.csv

A sample of 1000 cases, downloaded from
https://raw.githubusercontent.com/jakevdp/marathon-data/master/marathon-data.csv 


## Obama-approval

Approval rate of the U.S. president Barack Obama from mid-January till
mid-May 2016.  Data from
[RealClearPolitics](https://www.realclearpolitics.com/).

Columns:

* **poll**: poll name
* **date**: poll date range
* **sample**: sample size
* **approve**: approval rate, pct
* **disapprove**: disapproval rate, pct
* **spread**: approval - disapproval


## Treatment

Originates from R package _Ecdat_.  A U.S. dataset from 1974, used
for
evaluating treatment effect of training on earnings.  

* **treat** treated ?
* **age** age
* **educ** education in years
* **ethn** a factor with levels ("‘other’", "‘black’", "‘hispanic’")
* **married** married ?
* **re74** real annual earnings in 1974 (pre-treatment)
* **re75** real annual earnings in 1975 (pre-treatment)
* **re78** real annual earnings in 1978 (post-treatment)
* **u74** unemployed in 1974 ?
* **u75** unemployed in 1975 ?

Source: Lalonde, R. (1986) “Evaluating the Econometric Evaluations of
Training Programs with Experimental Data”, _American Economic
Review_, 604-620.
