# Lecturenotes

A collection of notes for data science and machine learning related
classes.  It is focused on topics where I haven't found a better
source.

It contains three texts:

* [Intro to data science and machine learning](https://faculty.washington.edu/otoomet/machineLearning.pdf)
* [Doing data science in python](https://faculty.washington.edu/otoomet/machinelearning-py/)
* [Doing data science in R](https://faculty.washington.edu/otoomet/machinelearning-R/)
* [Intro to Data Science](https://faculty.washington.edu/otoomet/datascience-intro/)

All texts are licensed as [CC BY
4.0](https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1).
The image copyrights vary--most images are Public Domain [CC0
1.0](https://creativecommons.org/publicdomain/zero/1.0/?ref=chooser-v1),
unless marked otherwise.  See the readme files in the corresponding
repo folder.
