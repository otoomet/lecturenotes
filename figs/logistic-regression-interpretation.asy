import patterns;
import graph;
import math;

size(130mm, 60mm, IgnoreAspect);
defaultpen(fontsize(8pt));

real logit(real x) {
  real z = 1/(1 + exp(-x));
  return z;
}

/* logistic curve graph using 'graph' */
guide logisticCurve = graph(logit, -4, 4);
draw(logisticCurve, linewidth(1));
xaxis(Label("$\eta$", align=N),
      linewidth(0.7), Arrow(TeXHead),
      ticks=RightTicks(endlabel=false));
yaxis(L=rotate(0)*"$\Pr(Y=1)$", linewidth(0.7), Arrow(TeXHead),
      ymin=0, ymax=1.1,
      ticks=LeftTicks(beginlabel=false,
		      N = 0, Step = 0.5));

/* Mark slope examples */
real[] xs = { -2.5, 0.5};
pen dxcolor = rgb("F8766D");
pen dycolor = rgb("00BFC4");
for(int i = 0; i < xs.length; ++i) {
  /* where to place markers */
  real markerx = xs[i] + 1.1;
  pair labelLoc = E;
  real dyMarkerStart;
  if(xs[i] < 0) {
    dyMarkerStart = xs[i];
  } else {
    dyMarkerStart = xs[i] + 1.1;
  }
  /* mark the point */
  pair xy = (xs[i], logit(xs[i]));
  dot(xy);
  label("$\eta_{" + string(i+1) + "} = (" + string(xy.x, 2) + "," + string(xy.y, 2) + ")$",
	xy, NW, Fill(white));
  pair lower = (xs[i], 0.0);
  path vertical = lower--(xs[i],1);
  pair upper = intersectionpoint(vertical, logisticCurve);
  bool deltaxTop = upper.y < 0.5;  // mark delta-x on top
  pair markerTop = upper;
  // how far up to draw the vertical delta-x lines
  if(deltaxTop) {
    markerTop = markerTop + (0, 0.3);
  }
  draw(lower--markerTop, dotted);
  real alpha = slope(logisticCurve, xs[i]);
  pair slopeUpper = (xs[i] + 1, upper.y + alpha);
  /* mark the derivative */
  draw(upper--slopeUpper, Arrow(TeXHead));  
  /* vertical line at the end of slope line */
  if(deltaxTop) {
    draw((slopeUpper.x, markerTop.y)--(slopeUpper.x, 0), dotted);
  } else {
    draw(slopeUpper--(slopeUpper.x, 0), dotted);
  }
  /* horizontal lines for deltay */
  draw((0, upper.y)--(dyMarkerStart, upper.y), dotted);
  draw((0, slopeUpper.y)--(dyMarkerStart, slopeUpper.y), dotted);
  /* -- mark delta x, delta y --- */
  real deltaxLevel = markerTop.y - 0.1;
  path deltaxArrow = (xs[i], deltaxLevel)--(xs[i]+1, deltaxLevel);
  draw(deltaxArrow, dxcolor, Arrow(TeXHead));
  label("$\Delta \eta = 1$", relpoint(deltaxArrow, 0.5), N);
  /* delta-y: either outside or inside */
  real deltay = slopeUpper.y - upper.y;
  bool markInner = deltay > 0.1;
  path deltayMarker;
  if(markInner) {
    deltayMarker = (markerx, upper.y)--(markerx, slopeUpper.y);
    draw(deltayMarker, dycolor, Arrows(TeXHead));
  } else {
    path upperDYMarker = (markerx, slopeUpper.y+0.1)--(markerx, slopeUpper.y);
    path lowerDYMarker = (markerx, upper.y-0.1)--(markerx, upper.y);
    deltayMarker = (markerx, upper.y)--(markerx, slopeUpper.y);
    draw(upperDYMarker, Arrow(TeXHead));
    draw(lowerDYMarker, Arrow(TeXHead));
    draw(deltayMarker, dycolor);
  }
  label("$\Delta P=" + string(deltay, 2) + "$",
  	relpoint(deltayMarker, 0.5), labelLoc, Fill(white));
}
