unitsize(9mm);

import patterns;
add("hatch",hatch(2mm, SE, red));
add("hatchback",hatch(2mm, NE, blue));

real size = 3;
pair cA = (-size, 0);
pair cB = (0.5size, 0);
real center = (cA.x + cB.x)/2;
path A = ellipse(cA, 1.5*size, 1.1*size);
path B = ellipse(cB, 1.5*size, 1.1*size);
path SS = box((cA.x - 1.6*size, 1.4*size), (cB.x + 1.6*size, -1.2*size));

filldraw(A, pattern("hatch"), red);
filldraw(B, pattern("hatchback"), blue);
draw(SS);

label("Survived (500)", cA, NW, Fill(white));
label("Female (466)", cB, NE, Fill(white));
label("Survived and female", (center, -0.1size), S, Fill(white));
label("(339)", (center,-0.1size), 6S, Fill(white));
label("Male, did not survive (682)", point(SS, 0), SE);
label("All passengers (1309)", (center, 1.4*size), N);
