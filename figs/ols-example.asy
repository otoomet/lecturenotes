/* */
import patterns;
texpreamble("\input{../isomath}");
size(80mm);
picture pic = new picture;

defaultpen(fontsize(9));
pen bgcolor = palegray;
pen errorColor = lightred;
pen predictionColor = mediumgray;

/* axes and such */
real ticklength = 0.05;
real y0 = 0.6;  // y axis height
path xaxis = (-0.1, y0)--(2.5, y0);
draw(pic, xaxis, Arrow(TeXHead));
path yaxis = (0, y0 - 0.1)--(0, 2.1);
draw(pic, yaxis, Arrow(TeXHead));
label(pic, "$0$", (0, y0), SW);
for(int x = 1; x < point(xaxis, 1).x; ++x) {
  draw(pic, (x, y0)--(x, y0 - ticklength));
  label(pic, string(x), (x, y0 - ticklength), S);
}
for(int y = 1; y < point(yaxis, 1).y; ++y) {
  draw(pic, (0, y)--(-ticklength, y));
  label(pic, string(y), (-ticklength, y), W);
}
label(pic, "$y$", point(yaxis, 1), SE);
label(pic, "$x$", point(xaxis, 1), E);

real extendDelta = 0.2;  // how much to draw lines over axis line
pair[] datapoints = {(0,1), (1,1), (2,2)};
// this gives beta0 = 0, beta1 = 1
int imin = 1;
int imax = 2;
real beta0 = 5/6;
real beta1 = 1/2;

/* Residual lines (lines underneath points)*/
path[] e;
real[] yhat;
for(int i = 0; i < datapoints.length; ++i) {
  real x = datapoints[i].x;
  yhat[i] = beta0 + beta1*x;
  e[i] = datapoints[i]--(x, yhat[i]);
  draw(pic, e[i], errorColor + linewidth(1));
}

/* Regression line */
real xStart = -0.05;
real xEnd = point(xaxis, 1).x - 0.1;
path regLine = (xStart,beta0 + beta1*xStart)--(xEnd, beta0 + beta1*xEnd);
draw(pic, regLine, linewidth(1.2));

/* Residual label: on top of regression line */
for(int i = 0; i < datapoints.length; ++i) {
  real x = datapoints[i].x;
  e[i] = datapoints[i]--(x, yhat[i]);
  label(pic, "$\epsilon_" + string(i) + "$",
	relpoint(e[i], 0.5), E, errorColor,
	Fill(bgcolor));
}

/* datapoints */
dotfactor=11;
pair[] dpLabelDir = {2*NE, 2*E, 2*W};
pair[] hatLabelDir = {2*SE, 2*NW, 2*SE};
for(int i = 0; i < datapoints.length; ++i) {
  dot(pic, datapoints[i]);  // datapoint
  label(pic, "$\vec{p}_" + string(i+1) + "=(" +
	string(datapoints[i].x) + "," + string(datapoints[i].y) + ")$",
	datapoints[i], dpLabelDir[i],
	Fill(bgcolor));
  dot(pic, (datapoints[i].x, yhat[i]), predictionColor);  // predicted
  label(pic, "$\hat{\vec{p}}_" + string(i+1) + "$",
	(datapoints[i].x, yhat[i]), hatLabelDir[i],
	gray, Fill(bgcolor));
}

add(pic, Fill(bgcolor));
