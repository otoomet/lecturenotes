unitsize(10mm);

import patterns;
add("hatch",hatch(2mm, SE, red));
add("hatchback",hatch(2mm, NE, blue));

real size = 3;
pair cA = (-size, 0);
pair cB = (size, 0);
path A = ellipse(cA, 1.5*size, 1.1*size);
path B = ellipse(cB, 1.5*size, 1.1*size);
path SS = box((cA.x - 1.6*size, 1.2*size), (cB.x + 1.6*size, -1.2*size));

filldraw(A, pattern("hatch"), red);
filldraw(B, pattern("hatchback"), blue);
draw(SS);

label("Event $A$", cA, W, Fill(white));
label("Event $B$", cB, E, Fill(white));
label("Both $A$ and $B$", (0,0), Fill(white));
label("$A\cap B$", (0,0), 10S, Fill(white));
label("Sample space $S$", (0, 1.2*size), N);
