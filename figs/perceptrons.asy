// AND perceptron
currentpicture = new picture;
size(120mm,0);
defaultpen(fontsize(10pt));
texpreamble("\input{../isomath}");

// nodes
pair x1loc = (-2.5,1);
path x1 = circle(x1loc, 0.3);
draw(x1);
label("$x_1$", x1loc);

pair x2loc = (-2.5,-1);
path x2 = circle(x2loc, 0.3);
draw(x2);
label("$x_2$", x2loc);

pair yloc = (0,0);
path y = circle(yloc, 0.9);
draw(y);
label("$z = w_1 x_1 + w_2 x_2$", yloc);
label("(linear transformation)", yloc, 3*S, red);

pair outloc = (2,0);
label(" Output", outloc, 2*E);
real layerLwd = 0.85;  // how think layer frames

// connecting arrows
path x1y = x1loc--yloc;
path x1yarrow = intersectionpoint(x1y, x1)--intersectionpoint(x1y, y);
draw(x1yarrow, Arrow(TeXHead));
label("$w_1 x_1$", midpoint(x1yarrow), 2N);

path x2y = x2loc--yloc;
path x2yarrow = intersectionpoint(x2y, x2)--intersectionpoint(x2y, y);
draw(x2yarrow, Arrow(TeXHead));
label("$w_2 x_2$", midpoint(x2yarrow), 2N);

path yOut = yloc--outloc;
path yOutArrow = intersectionpoint(yOut, y)--outloc;
draw(yOutArrow, Arrow(TeXHead));
label("$\indic(z > \bar z)$", midpoint(yOutArrow), N);
label("(activation)", midpoint(yOutArrow), S, red);

// Layers
frame inputFrame = newframe;
draw(inputFrame, x1);
draw(inputFrame, x2);
path inputLayer = box(inputFrame, -0.4, -0.4);
draw(inputLayer, dashed+blue+linewidth(layerLwd));
label("Input layer", point(inputLayer, 3), NE, blue);

frame outputFrame = newframe;
draw(outputFrame, y);
draw(outputFrame, outloc);
path outputLayer = box(outputFrame, -0.4, -0.4);
draw(outputLayer, dashed+blue+linewidth(layerLwd));
label("Output layer", point(outputLayer, 3), NE, blue);
//
shipout("and-perceptron");

// ---------- XOR perceptron ----------
currentpicture = new picture;
size(120mm,0);
defaultpen(fontsize(9pt));
real nodesize=0.8;

// nodes
pair x1loc = (-2.5,1);
path x1 = circle(x1loc, 0.3);
draw(x1);
label("$x_1$", x1loc);

pair x2loc = (-2.5,-1);
path x2 = circle(x2loc, 0.3);
draw(x2);
label("$x_2$", x2loc);

pair h1loc = (0,1.2);
path h1 = circle(h1loc, nodesize);
draw(h1);
label("$\chi_1 = \vec{x}^{\transpose} \cdot \vec{w_{h1}}$", h1loc, N);
label("$h_1 = \indic(\chi_1 > b_{h1})$", h1loc, S);

pair h2loc = (0,-1.2);
path h2 = circle(h2loc, nodesize);
draw(h2);
label("$\chi_2 = \vec{x}^{\transpose} \cdot \vec{w_{h2}}$", h2loc, N);
label("$h_2 = \indic(\chi_2 > b_{h2})$", h2loc, S);

pair yloc = (2.5,0);
path y = circle(yloc, nodesize);
draw(y);
label("$z = \vec{h}^{\transpose} \cdot\vec{w}_y$", yloc, N);
label("$y = \indic(z > b_y)$", yloc, S);

pair outloc = (4,0);

// connecting arrows
path x1h1 = x1loc--h1loc;
path x1h1arrow = intersectionpoint(x1h1, x1)--intersectionpoint(x1h1, h1);
draw(x1h1arrow, Arrow(TeXHead));
label("$x_1\cdot w_{h11}$", midpoint(x1h1arrow), N);
path x1h2 = x1loc--h2loc;
path x1h2arrow = intersectionpoint(x1h2, x1)--intersectionpoint(x1h2, h2);
draw(x1h2arrow, Arrow(TeXHead));
label("$x_1\cdot w_{h21}$", intersectionpoint(x1h2, h2), W);
path x2h1 = x2loc--h1loc;
path x2h1arrow = intersectionpoint(x2h1, x2)--intersectionpoint(x2h1, h1);
draw(x2h1arrow, Arrow(TeXHead));
label("$x_2\cdot w_{h12}$", intersectionpoint(x2h1, h1), W);
path x2h2 = x2loc--h2loc;
path x2h2arrow = intersectionpoint(x2h2, x2)--intersectionpoint(x2h2, h2);
draw(x2h2arrow, Arrow(TeXHead));
label("$x_2\cdot w_{h22}$", midpoint(x2h2arrow), S);

path h1y = h1loc--yloc;
path h1yarrow = intersectionpoint(h1y, h1)--intersectionpoint(h1y, y);
draw(h1yarrow, Arrow(TeXHead));
label("$h_1\cdot w_{y1}$", intersectionpoint(h1y, y), SW);
path h2y = h2loc--yloc;
path h2yarrow = intersectionpoint(h2y, h2)--intersectionpoint(h2y, y);
draw(h2yarrow, Arrow(TeXHead));
label("$h_2\cdot w_{y2}$", intersectionpoint(h2y, y), NW);

path yOut = yloc--outloc;
path yOutArrow = intersectionpoint(yOut, y)--outloc;
draw(yOutArrow, Arrow(TeXHead));
label("Output", midpoint(yOutArrow), N);
label("$y$", midpoint(yOutArrow), S);

// Layers
frame inputFrame = newframe;
draw(inputFrame, x1);
draw(inputFrame, x2);
path inputLayer = box(inputFrame, -0.4, -0.4);
draw(inputLayer, dashed+blue+linewidth(layerLwd));
label("Input layer", point(inputLayer, 3), NE, blue);

frame hiddenFrame = newframe;
draw(hiddenFrame, h1);
draw(hiddenFrame, h2);
path hiddenLayer = box(hiddenFrame, -0.4, -0.4);
draw(hiddenLayer, dashed+red+linewidth(layerLwd));
label("Hidden layer", point(hiddenLayer, 3), NE, red);

frame outputFrame = newframe;
draw(outputFrame, y);
draw(outputFrame, outloc);
path outputLayer = box(outputFrame, -0.4, -0.4);
draw(outputLayer, dashed+heavygreen+linewidth(layerLwd));
label("Output layer", point(outputLayer, 3), NE, heavygreen);

// done
shipout("xor-perceptron");


/* ---------- multi-layer perceptron ---------- */
currentpicture = new picture;
size(110mm,0);
defaultpen(fontsize(9pt));

/* graphical params */
real nodesize=0.3;
real nodegap = 1;  // gap b/w nodes
real layergap = 1.5;

int nx = 4;  // input nodes
int nhiddens[] = {5, 4}; //hidden layers
int ny = 2;  

// input layer
real xx = 0;
path[] xnode;
pair[] xcenter;
int xi = 1;
for(real xy = (nx - 1)/2*nodegap; xi <= nx; xy = xy - nodegap) {
  xcenter[xi-1] = (xx, xy);
  xnode[xi-1] = circle(xcenter[xi-1], nodesize);
  draw(xnode[xi-1]);
  label("$x_" + string(xi) + "$", xcenter[xi-1]);
  ++xi;
}

/* hidden layers */
path[] hnode[];
pair[] hcenter[];
for(int hi=1; hi <= nhiddens.length; ++hi) {
  real hx = hi*layergap;
  int ihnode = 1;
  hnode.push(new path[]);
  hcenter.push(new pair[]);
  for(real hy = (nhiddens[hi-1]-1)/2*nodegap; ihnode <= nhiddens[hi-1]; hy = hy - nodegap) {
    hcenter[hi-1].push((hx, hy));
    hnode[hi-1].push(circle(hcenter[hi-1][ihnode-1], nodesize));
    draw(hnode[hi-1][ihnode-1]);
    label("$h_{" + string(hi) + string(ihnode) + "}$", hcenter[hi-1][ihnode-1]);
    ++ihnode;
  }
}

/*  output layer */
real xy = (1 + nhiddens.length)*layergap;
path[] ynode;
pair[] ycenter;
int yi = 1;
for(real yy = (ny - 1)/2*nodegap; yi <= ny; yy = yy - nodegap) {
  ycenter[yi-1] = (xy, yy);
  ynode[yi-1] = circle(ycenter[yi-1], nodesize);
  draw(ynode[yi-1]);
  label("$y_" + string(yi) + "$", ycenter[yi-1]);
  ++yi;
}

/* arrows from input to h1 */
for(int xi = 1; xi <= nx; ++xi) {
  for(int hi = 1; hi <= nhiddens[0]; ++hi) {
    path line = xcenter[xi-1]--hcenter[0][hi-1];
    draw(intersectionpoint(line, xnode[xi-1])--
	 intersectionpoint(line, hnode[0][hi-1]),
	 Arrow(TeXHead));
  }
}

/* arrows in hidden layers */
for(int h1i = 1; h1i < nhiddens.length; ++h1i) {
  // 'h1i': layer to draw from
  for(int h1ni = 1; h1ni <= nhiddens[h1i-1]; ++h1ni) {
    // 'h2i': layer to draw to
    for(int h2ni = 1; h2ni <= nhiddens[h1i]; ++h2ni) {
      path line = hcenter[h1i-1][h1ni-1]--hcenter[h1i][h2ni-1];
      draw(intersectionpoint(line, hnode[h1i-1][h1ni-1])--
	   intersectionpoint(line, hnode[h1i][h2ni-1]),
	   Arrow(TeXHead));
    }
  }
}

/* arrows from last hiddent to output */
for(int hi = 1; hi <= nhiddens[nhiddens.length-1]; ++hi) {
  for(int yi = 1; yi <= ny; ++yi) {
    path line = hcenter[nhiddens.length-1][hi-1]--ycenter[yi-1];
    draw(intersectionpoint(line, hnode[nhiddens.length-1][hi-1])--
	 intersectionpoint(line, ynode[yi-1]),
	 Arrow(TeXHead));
  }
}

//  Layer markers
frame inputFrame = newframe;
for(int xi = 1; xi <= nx; ++xi) {
  draw(inputFrame, xnode[xi-1]);
}
path inputLayer = box(inputFrame, -0.4, -0.4);
draw(inputLayer, dashed+blue+linewidth(layerLwd));
label("Input layer", (xcenter[0].x, point(inputLayer, 3).y), N, blue);

/* hidden layers */
for(int ihlayer=1; ihlayer <= nhiddens.length; ++ihlayer) {
  frame hiddenFrame = newframe;
  for(int ih = 1; ih <= nhiddens[ihlayer-1]; ++ih) {
    draw(hiddenFrame, hnode[ihlayer-1][ih-1]);
  }
  path hiddenLayer = box(hiddenFrame, -0.4, -0.4);
  draw(hiddenLayer, dashed+red+linewidth(layerLwd));
  label("Hidden layer " + string(ihlayer),
	(hcenter[ihlayer-1][0].x, point(hiddenLayer, 3).y), N, red);
}

/* output layer */
frame outputFrame = newframe;
for(int yi = 1; yi <= ny; ++yi) {
  draw(outputFrame, ynode[yi-1]);
}
path outputLayer = box(outputFrame, -0.4, -0.4);
draw(outputLayer, dashed+heavygreen+linewidth(layerLwd));
label("Output layer", (ycenter[0].x, point(outputLayer, 3).y), N, heavygreen);

// done
shipout("multi-layer-perceptron");
