# Image sources and copyrights

Images, not listed here, are made by Ott Toomet and licensed as Public
Domain
[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/?ref=chooser-v1). 


* **2d_gradient_descent.jpg**,
 **2d_gradient_descent_local_minimum.jpg**, **2d_parabola.jpg**: these
 images are from an unknown source and may be copyrighted.  Do not
 reuse.  Will be removed from here at some point.
