defaultpen(fontsize(8));  // applies to all code

/* figure in plain rating space */
currentpicture = new picture;
unitsize(13mm);

real tick = 0.1;
path xaxis = (-0.2,0)--(5.5,0);
path yaxis = (0,-0.2)--(0, 4.5);
draw(xaxis, Arrow(HookHead));
draw(yaxis, Arrow(HookHead));
label("Rating: Under the Bed", xaxis, 6*S);
label(rotate(90)*"Rating: The Monk", yaxis, 5*W);

// coordinate ticks
for(real i = 1; i < relpoint(xaxis, 1).x; ++i) {
  draw((i,0)--(i,-tick));
  label(string(i), (i,-tick), S);
}
for(real i = 1; i < relpoint(yaxis, 1).y; ++i) {
  draw((0,i)--(-tick,i));
  label(string(i), (-tick, i), W);
}

// ratings
pair ji = (1,4);
pair chen = (3,1);
pair su = (4,1);
// draw ratings
pair[] raters = {ji, chen, su};
for(pair rater : raters) {
  draw((0,0)--rater);
  draw((0,rater.y)--rater--(rater.x,0), dotted);
  dot(rater);
}
label("Ji", ji, E);
label("Chen", chen, N);
label("Su", su, E);

// angles b/w raters
real aJi = degrees(atan2(ji.y, ji.x));
real aChen = degrees(atan2(chen.y, chen.x));
real aSu = degrees(atan2(su.y, su.x));
path jiChen = arc((0,0), 0.3, aChen, aJi);
draw(jiChen);
path suChen = arc((0,0), 0.5, aSu, aChen);
draw(suChen);

shipout("2d-movie-ratings-standard");

/*
  ----------------------
  centered feature space
  ----------------------
*/
currentpicture = new picture;
unitsize(15mm, 0);

// centered ratings
pair ji = (-1.5, 1.5);
pair chen = (1.0, -1.0);
pair su = (1.52, -1.49);
// draw ratings
pair[] raters = {ji, chen, su};
for(pair rater : raters) {
  draw((0,0)--rater);
  draw((0,rater.y)--rater--(rater.x,0), dotted);
  dot(rater);
}
label("Ji", ji, NE);
label("Chen", chen, NE);
label("Su", su, NE);

real tick = 0.05;
path xaxis = (-1.7, 0)--(1.7,0);
path yaxis = (0, -1.7)--(0, 1.7);
draw(xaxis, Arrow(HookHead));
draw(yaxis, Arrow(HookHead));
label("Under the Bed", point(xaxis, 1), 3*NW);
label(rotate(90)*"The Monk", point(yaxis, 1), 3*SE);

// coordinate ticks
for(real i = -1.5; i < relpoint(xaxis, 1).x; i += 0.5) {
  if(abs(i) < 0.01)
    continue;
  draw((i,0)--(i,-tick));
  label(string(i), (i,-tick), S);
}
for(real i = -1.5; i < relpoint(yaxis, 1).y; i += 0.5) {
  if(abs(i) < 0.01)
    continue;
  draw((0,i)--(-tick,i));
  label(string(i), (-tick, i), W);
}

// angles b/w raters
real aJi = degrees(atan2(ji.y, ji.x));
real aChen = degrees(atan2(chen.y, chen.x));
real aSu = degrees(atan2(su.y, su.x));
path jiChen = arc((0,0), 0.1, aChen, aJi);
draw(jiChen);

shipout("2d-movie-ratings-centered");

