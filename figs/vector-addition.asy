usepackage("amsbsy");
unitsize(4mm);
defaultpen(fontsize(8));

pair x = (-1, 3);
pair y = (5, -2);

dot((0,0));
draw((0,0)--x, mediumred, Arrow);
label("$\boldsymbol{x}$", x, NW);
draw((0,0)--y, mediumblue, Arrow);
label("$\boldsymbol{y}$", y, SW);

draw((0,0)--2*x, dotted+mediumred, Arrow);
label("$2\boldsymbol{x}$", 2*x, NW);
draw((0,0)--3*y, dotted+mediumblue, Arrow);
label("$3\boldsymbol{y}$", 3*y, SW);

pair z = 2*x + 3*y;
draw((0,0)--z, darkgreen, Arrow);
label("$\boldsymbol{z} = 2\boldsymbol{x} + 3\boldsymbol{y}$", z, SW);
draw(2*x--z, dotted);
draw(3*y--z, dotted);
