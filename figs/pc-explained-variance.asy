/* */
import patterns;
texpreamble("\input{../isomath}");

pen pc1color = rgb("FDE735FF");
pen pc2color = rgb("440154FF");
pen bgcolor = palegray;
real circleSize = 0.18;
pair[] datapoints = { (2.5,0.5), (5,6), (7.5,5.5) };

currentpicture = new picture;
size(130mm,0);
defaultpen(fontsize(8pt));

path axes(pair origin = (0,0)) {
  path a = (origin.x, origin.y+9)--origin--(origin.x+9, origin.y);
  return a;
}

void drawAxes(picture pic=currentpicture, path axes,
	      bool labels=true) {
  pair[] directions = { dir(0), dir(90), dir(45) };
  draw(pic, axes, Arrows(TeXHead));
  label(pic, "$x_2$", point(axes, 0), 3*SW);
  label(pic, "$x_1$", point(axes, 2), 6*SW);
  int i = 0;
  for(pair datapoint : datapoints) {
    path d = circle(datapoint, circleSize);
    filldraw(pic, d, lightgray);
    if(labels) {
      label(pic, "$d_" + string(i+1) + "$", datapoint, 4*directions[i]);
    }
    ++i;
  }
}

/* -------------------- total variation in data -------------------- */

picture pic = new picture;
path axes = axes();
drawAxes(pic, axes);

/* centroid */
pair centroid = (0,0);
for(pair d : datapoints) {
  centroid += d;
}
centroid /= datapoints.length;
path centroidCircle = circle(centroid, circleSize);
filldraw(pic, centroidCircle, black);
label(pic, "Centroid", centroid, 4*E);

real V = 0;  // total variance
/* draw distance to the centroid */
pair[] directions = { dir(180), dir(180), dir(0) };
for(int i = 0; i < datapoints.length; ++i) {
  path d = circle(datapoints[i], circleSize);
  path e = datapoints[i]--centroid;
  draw(pic, intersectionpoint(e, centroidCircle)--intersectionpoint(e, d),
       Arrows(TeXHead));
  label(pic, "$e_" + string(i+1) + "=" + string(arclength(e), 4) + "$", relpoint(e, 0.5), 3*directions[i]);
  V += arclength(e)^2;
}
label(pic, "Total variation $V=" + string(V, 4) + "$", point(axes, 0), 2*SE);

add(pic, Fill(bgcolor));


/* -------------------- variation explained by PC1 -------------------- */
picture pic = new picture;
path axes = axes();
drawAxes(pic, axes, labels=false);

/* PC1, at 45deg angle, passes the centroid  */
pair pc1dir = dir(45);
pair pc2dir = dir(135);
path pc1 = (centroid - 5*pc1dir)--(centroid + 5*pc1dir);
draw(pic, pc1, pc1color + linewidth(1), Arrow(TeXHead));
label(pic, "$\mathit{PC}_1$", point(pc1, 1), SE);
/* PC2, at -45deg angle, passes the centroid  */
path pc2 = (centroid - 5*pc2dir)--(centroid + 4*pc2dir);
draw(pic, pc2, pc2color + linewidth(1), Arrow(TeXHead));
label(pic, "$\mathit{PC}_2$", point(pc2, 1), W);
/* mark distance along pc1 */
real V1 = 0, V2 = 0;
pair[] directions1 = { dir(315), dir(130), dir(0) };
pair[] directions2 = { dir(200), dir(45), dir(40) };
path centroidmarker = (centroid - pc2dir)--(centroid + 1.7*pc2dir);
draw(pic, centroidmarker, dashed + pc2color);
for(int i = 0; i < datapoints.length; ++i) {
  path dpcircle = circle(datapoints[i], circleSize);
  /* centroid to point */
  path e = datapoints[i]--centroid;
  draw(pic, intersectionpoint(e, centroidCircle)--intersectionpoint(e, dpcircle),
       dotted, Arrows(TeXHead));
  label(pic, "$e_" + string(i+1) + "$", relpoint(e, 0.5), Fill(bgcolor));
  pair ddir = datapoints[i] - centroid;
  /* PC1 explained variation */
  pair crosspoint = centroid + dot(pc1dir, ddir)*pc1dir;
  path e = datapoints[i]--(datapoints[i] + dot(centroid - datapoints[i], pc1dir)*pc1dir);
  draw(pic, intersectionpoint(e, dpcircle)--point(e, 1), pc1color, Arrows(TeXHead));
  label(pic, "$" + string(arclength(e), 4) + "$",
	relpoint(e, 0.5), 2*directions1[i],
	Fill(bgcolor));
  V1 += arclength(e)^2;
  /* PC2 explained variation */
  pair crosspoint = centroid + dot(pc2dir, ddir)*pc2dir;
  path e = datapoints[i]--(datapoints[i] + dot(centroid - datapoints[i], pc2dir)*pc2dir);
  draw(pic, intersectionpoint(e, dpcircle)--point(e, 1), pc2color, Arrows(TeXHead));
  label(pic, "$" + string(arclength(e), 4) + "$",
	relpoint(e, 0.5), 2*directions2[i],
	Fill(bgcolor));
  V2 += arclength(e)^2;
}
label(pic, "Variation explained by $\mathit{PC}_1$: $V_1=" + string(V1, 4) + "$", point(axes, 0), 2*SE);
label(pic, "Variation explained by $\mathit{PC}_2$: $V_2=" + string(V2, 4) + "$", relpoint(axes, 0.05), 2*E);
filldraw(pic, centroidCircle, black);

add(shift((11,0))*pic, Fill(bgcolor));
