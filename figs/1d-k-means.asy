// Demo of 1-d k-means clusters and loss function
defaultpen(fontsize(8pt));
texpreamble("\input{../isomath}");
usepackage("amsbsy");

real figwidth=120mm;
currentpicture = new picture;
size(figwidth,0);

/* data points */
real datapoints[] = {-1, 0, 1, 3, 4 };
real y = 0.5;
int badMembership[] = {1,1,2, 2,2};
int goodMembership[] = {1,1,1, 2,2};
real r = 0.05;
pen color[] = {black, lightred, green};

real ticklength=0.1;
real distanceshift=3*r;

void draw(int[] membership) {
  /* x-axis */
  path xaxis = (min(datapoints) - 0.5, 0)--(max(datapoints) + 0.5,0);
  draw(xaxis, Arrow);
  label("$x$", point(xaxis, 1), 2*SW);
  /* data points */
  for(int i = 0; i < datapoints.length; ++i) {
    int cluster = membership[i];
    real x = datapoints[i];
    fill(circle((x, y), r), color[cluster - 1]);
    draw(circle((x, y), r));
    /* labels */
    label("$x_"+string(i+1)+"$", (x, y+r), N);
    /* ticks */
    draw((x, 0)--(x, -ticklength));
    label(string(x), (x, -ticklength), S);
  }
  /* labels */
  label("Data points", (min(datapoints), y), 10*W);
}

/* Good memberships */
draw(goodMembership);

/* distances */
path d12 = (datapoints[0],y-distanceshift)--(datapoints[1], y-distanceshift);
draw(d12, Arrows(TeXHead));
label("$d_{12}=" + string(arclength(d12)) + "$",
      relpoint(d12, 0.5), N);
path d23 = (datapoints[1],y-distanceshift)--(datapoints[2], y-distanceshift);
draw(d23, Arrows(TeXHead));
label("$d_{23}=" + string(arclength(d23)) + "$",
      relpoint(d23, 0.5), N);
path d31 = (datapoints[2],y-2*distanceshift)--(datapoints[0], y-2*distanceshift);
draw(d31, Arrows(TeXHead));
label("$d_{31}=" + string(arclength(d31)) + "$",
      relpoint(d31, 0.5), S);
path d45 = (datapoints[3],y-distanceshift)--(datapoints[4], y-distanceshift);
draw(d45, Arrows(TeXHead));
label("$d_{45}=" + string(arclength(d45)) + "$",
      relpoint(d45, 0.5), N);
/* distance location marks */
draw((datapoints[0], y-0.5*distanceshift)--(datapoints[0], y-2.5*distanceshift));
draw((datapoints[1], y-0.5*distanceshift)--(datapoints[1], y-1.5*distanceshift));
draw((datapoints[2], y-0.5*distanceshift)--(datapoints[2], y-2.5*distanceshift));
draw((datapoints[3], y-0.5*distanceshift)--(datapoints[3], y-1.5*distanceshift));
draw((datapoints[4], y-0.5*distanceshift)--(datapoints[4], y-1.5*distanceshift));

shipout("1d-k-means-bad");

currentpicture = new picture;
size(figwidth,0);

draw(badMembership);

/* distances */
/* Black cluster */
path d12 = (datapoints[0],y-distanceshift)--(datapoints[1], y-distanceshift);
draw(d12, Arrows(TeXHead));
label("$d_{12}=" + string(arclength(d12)) + "$",
      relpoint(d12, 0.5), N);
/* red cluster */
path d34 = (datapoints[2],y-distanceshift)--(datapoints[3], y-distanceshift);
draw(d34, Arrows(TeXHead));
label("$d_{34}=" + string(arclength(d34)) + "$",
      relpoint(d34, 0.5), N);
path d45 = (datapoints[3],y-distanceshift)--(datapoints[4], y-distanceshift);
draw(d45, Arrows(TeXHead));
label("$d_{45}=" + string(arclength(d45)) + "$",
      relpoint(d45, 0.5), N);
path d53 = (datapoints[4],y-2*distanceshift)--(datapoints[2], y-2*distanceshift);
draw(d53, Arrows(TeXHead));
label("$d_{53}=" + string(arclength(d53)) + "$",
      relpoint(d53, 0.5), S);
/* distance location marks */
draw((datapoints[0], y-0.5*distanceshift)--(datapoints[0], y-1.5*distanceshift));
draw((datapoints[1], y-0.5*distanceshift)--(datapoints[1], y-1.5*distanceshift));
draw((datapoints[2], y-0.5*distanceshift)--(datapoints[2], y-2.5*distanceshift));
draw((datapoints[3], y-0.5*distanceshift)--(datapoints[3], y-1.5*distanceshift));
draw((datapoints[4], y-0.5*distanceshift)--(datapoints[4], y-2.5*distanceshift));

shipout("1d-k-means-good");
