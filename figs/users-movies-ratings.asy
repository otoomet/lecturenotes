defaultpen(fontsize(8));  // applies to all code

/* figure in plain rating space */
currentpicture = new picture;
unitsize(23mm);

/* users */
pair suLoc = (0, 1);
pair chenLoc = (0, 0);
pair jiLoc = (0, -1);
real userSize = 0.3;  // size of user's circle

path su = circle(suLoc, userSize);
path chen = circle(chenLoc, userSize);
path ji = circle(jiLoc, userSize);
draw(su);
draw(chen);
draw(ji);
label("Su", suLoc);
label("Chen", chenLoc);
label("Ji", jiLoc);

/* movies */
pair utbLoc = (2, 0.5);
pair tmLoc = (2, -0.5);
real moviePadding = 5;

frame utbFrame = newframe;
unitsize(23mm);
//label(utbFrame, "Under the Bed", utbLoc);
label("Under the Bed", utbLoc);
path utb = box(utbFrame, Label("utb"));
//draw(utb);

// frame tmFrame = newframe;
// label(tmFrame, "The Monk", utbLoc);
// path tm = box(tmFrame, -0.4, -0.4);
// draw(tm);

/* connection lines */
path l1 = suLoc--utbLoc;
//draw(intersectionpoint(su, l1)--intersectionpoint(utb, l1), Arrow());

real tick = 0.1;
path xaxis = (-0.2,0)--(5.5,0);
path yaxis = (0,-0.2)--(0, 4.5);
draw(xaxis, Arrow(HookHead));
draw(yaxis, Arrow(HookHead));
label("Rating: Under the Bed", xaxis, 6*S);
label(rotate(90)*"Rating: The Monk", yaxis, 5*W);

// coordinate ticks
for(real i = 1; i < relpoint(xaxis, 1).x; ++i) {
  draw((i,0)--(i,-tick));
  label(string(i), (i,-tick), S);
}
for(real i = 1; i < relpoint(yaxis, 1).y; ++i) {
  draw((0,i)--(-tick,i));
  label(string(i), (-tick, i), W);
}

// centered ratings
pair ji = (-1.5, 1.5);
pair chen = (1.0, -1.0);
pair su = (1.52, -1.49);
// draw ratings
pair[] raters = {ji, chen, su};
for(pair rater : raters) {
  draw((0,0)--rater);
  draw((0,rater.y)--rater--(rater.x,0), dotted);
  dot(rater);
}
label("Ji", ji, NE);
label("Chen", chen, NE);
label("Su", su, NE);

real tick = 0.05;
path xaxis = (-1.7, 0)--(1.7,0);
path yaxis = (0, -1.7)--(0, 1.7);
draw(xaxis, Arrow(HookHead));
draw(yaxis, Arrow(HookHead));
label("Under the Bed", point(xaxis, 1), 3*NW);
