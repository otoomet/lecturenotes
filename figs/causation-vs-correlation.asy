/* two images: */
import patterns;
texpreamble("\input{../isomath}");

currentpicture = new picture;
size(130mm,0);
defaultpen(fontsize(8pt));
pen namepen = fontsize(6pt) + gray;

path axes(pair origin = (0,0)) {
  path a = (origin.x, origin.y+6)--origin--(origin.x+6, origin.y);
  return a;
}

path line(pair A, pair B, real xk, real xl) {
  // make a line determined by points A and B
  // from horizontal coordinates xk to xl
  real beta = (B.y - A.y)/(B.x - A.x);
  real yk = A.y + beta*(xk - A.x);
  real yl = A.y + beta*(xl - A.x);
  path line = (xk, yk)--(xl, yl);
  return(line);
}

real circleSize = 0.15;
real tickLength = 0.15;

real outcomeOffset = 0.8;  // how much outcome lifted above 0
real[] educations = {1, 1, 4, 4};
real deltaEdu = educations[2] - educations[0];
real[] pais = outcomeOffset + educations;
string[] names = {"Xuande", "Guan Yu", "Zhang Fei", "Cao Cao"};
pen[] personColors = {deepcyan, black, heavygreen, heavyred};
string[] checkers = {"xuande", "zhang-fei", "guan-yu", "cao-cao"};
pen anglePen = gray;
pen counterfactualPen = lightgray;  // for drawing counterfactual markers

void drawAxes(picture pic=currentpicture, path axes) {
  draw(pic, axes, Arrows(TeXHead));
  label(pic, rotate(90)*"Outcome $y$", point(axes, 0), 3*SW);
  label(pic, "Treatment", point(axes, 2), 6*SW);
  draw(pic, (educations[0], 0)--(educations[0], tickLength));
  draw(pic, (educations[2], 0)--(educations[3], tickLength));
  label(pic, "0", (educations[0], 0), S);
  label(pic, "1", (educations[2], 0), S);
}

/* ---------------------------------------------- */
/* ---------- mean independence ----------------- */
/* ---------------------------------------------- */
picture pic = new picture;
path axes = axes();
pair[] personsBefore, personsAfter;
real[] labelLoc = {0.7, 0.2};  // location of effect labels
real[] epss = {1, -0.9};  // epsilons
real[] ds = {0.9, 0.5};
/* axes and labels */
drawAxes(pic, axes);
label(pic, "Causal effect $\beta_1$", point(axes, 0), NE);
draw(pic, (educations[0], pais[0]+epss[0])--(educations[0], 0), anglePen + dotted);
draw(pic, (educations[0] + deltaEdu, pais[0] + deltaEdu + epss[0])--(educations[0] + deltaEdu, 0), anglePen + dotted);
for(int iperson = 0; iperson < 2; ++iperson) {
  pair personBefore = (educations[iperson],
		       pais[iperson] + epss[iperson]);
  personsBefore.push(personBefore);
  pair personAfter = (educations[iperson] + deltaEdu,
		      pais[iperson] + deltaEdu + epss[iperson]);
  personsAfter.push(personAfter);
  path xb = circle(personBefore, circleSize);
  path xa = circle(personAfter, circleSize);
  path effect = personBefore--personAfter;
  draw(pic, intersectionpoint(xb, effect)--intersectionpoint(xa, effect),
       Arrow(TeXHead));
}
/* add names */
label(pic, names[0], personsBefore[0], 8*N, namepen);
label(pic, names[1], personsBefore[1], 2*SE, namepen);
/* plot persons */
path[] bMarkers, aMarkers;
for(int iperson = 0; iperson < 2; ++iperson) {
  path bMarker = circle(personsBefore[iperson], circleSize);
  bMarkers.push(bMarker);
  filldraw(pic, bMarker, personColors[iperson]);
  add(checkers[iperson], checker(0.3mm, personColors[iperson]));
  path aMarker = circle(personsAfter[iperson], circleSize);
  filldraw(pic, aMarker, pattern(checkers[iperson]));
  aMarkers.push(aMarker);
}

/* mark midpoints w/o and w/treatment */
pair avgT0 = (personsBefore[0] + personsBefore[1])/2;
pair avgT1 = (personsAfter[0] + personsAfter[1])/2;
draw(pic, line(avgT0, avgT1, 0, avgT1.x+1), anglePen + dashed);
// extend the correlation line
path avgT0Marker = circle(avgT0, circleSize);
path avgT1Marker = circle(avgT1, circleSize);
fill(pic, avgT0Marker, anglePen);
add("midpoint", checker(0.3mm, anglePen));
filldraw(pic, avgT1Marker, pattern("midpoint"));
label(pic, "$\E[y|T=0]$", avgT0, 2*W, Fill(white));
label(pic, "$\E[y|T=1]$", avgT1, 4*E, Fill(white));
/* causal effect arrows */
path effect = avgT0--avgT1;
draw(pic, intersectionpoint(effect, avgT0Marker)--intersectionpoint(effect, avgT1Marker),
     anglePen, Arrow(TeXHead));
real d = 0.3;
path horizontal = avgT0--(avgT1.x + d, avgT0.y);
path vertical = (avgT1.x + d, avgT0.y)--(avgT1) + (d, 0);
draw(pic, horizontal, anglePen);
draw(pic, avgT1--(avgT1 + (d,0)), anglePen);
draw(pic, vertical, anglePen + dashed, Arrow(TeXHead));
label(pic, "$\beta_1$", relpoint(vertical, 0.5), E,
      anglePen, Fill(white));
/* mark epsilons */
path eps = avgT0--personsBefore[0];
eps = intersectionpoint(avgT0Marker, eps)--intersectionpoint(bMarkers[0], eps);
draw(pic, eps, personColors[0], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
path eps = avgT0--personsBefore[1];
eps = intersectionpoint(avgT0Marker, eps)--intersectionpoint(bMarkers[1], eps);
draw(pic, eps, personColors[1], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
path eps = avgT1--personsAfter[0];
eps = intersectionpoint(avgT1Marker, eps)--intersectionpoint(aMarkers[0], eps);
draw(pic, eps, personColors[0], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
path eps = avgT1--personsAfter[1];
eps = intersectionpoint(avgT1Marker, eps)--intersectionpoint(aMarkers[1], eps);
draw(pic, eps, personColors[1], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);

add(pic);

/* ---------- correlational datapoints, no bias ---------- */
picture pic = new picture;
pair dataOrigin = (10,0);
pen counterfactualColor = lightgray;
real[] epss = {0.7, -1, 1.1, -1.4};  // epsilons

path axes = axes();
/* axes and markers */
drawAxes(pic, axes);
label(pic, "Mean independence holds", point(axes, 0), NE);
draw(pic, (educations[0], pais[0]+epss[0])--(educations[0], 0), anglePen + dotted);
draw(pic, (educations[2], pais[2] + epss[2])--(educations[2], 0), anglePen + dotted);
path[] personMarkers;
pair[] persons;
for(int iperson = 0; iperson < names.length; ++iperson) {
  pair personBefore = (educations[iperson],
		       pais[iperson] + epss[iperson]);
  persons.push(personBefore);
  path xb = circle(persons[iperson], circleSize);
  personMarkers.push(xb);
}
/* mark midpoints w/o and w/treatment */
pair avgT0 = (persons[0] + persons[1])/2;
pair avgT1 = (persons[2] + persons[3])/2;
draw(pic, avgT0--avgT1, anglePen);
path avgT0Marker = circle(avgT0, circleSize);
path avgT1Marker = circle(avgT1, circleSize);
fill(pic, avgT0Marker, anglePen);
fill(pic, avgT1Marker, anglePen);
label(pic, "$\E[y|T=0]$", avgT0, 2*W, Fill(white));
label(pic, "$\E[y|T=1]$", avgT1, 4*E, Fill(white));
/* mark correlation effect */
real d = 0.3;
path horizontal = avgT0--(avgT1.x + d, avgT0.y);
path vertical = (avgT1.x + d, avgT0.y)--(avgT1) + (d, 0);
draw(pic, horizontal, anglePen);
draw(pic, avgT1--(avgT1 + (d,0)), anglePen);
draw(pic, vertical, anglePen + dashed, Arrow(TeXHead));
label(pic, "$\alpha_1 = \beta_1$", relpoint(vertical, 0.4), E,
      anglePen, Fill(white));
/* mark the causal relationship and the arrows */
path causal = line(avgT0, avgT1, 0, avgT1.x+1);
draw(pic, causal, anglePen + dashed);
path effect = persons[0]--(persons[0] + deltaEdu*(1,1));
path personAfter = circle(point(effect, 1), circleSize);
fill(pic, personAfter, counterfactualColor);
draw(pic, intersectionpoint(effect, personMarkers[0])--
     intersectionpoint(effect, personAfter),
     counterfactualColor, Arrow(TeXHead));
effect = persons[1]--(persons[1] + deltaEdu*(1,1));
personAfter = circle(point(effect, 1), circleSize);
fill(pic, personAfter, counterfactualColor);
draw(pic, intersectionpoint(effect, personMarkers[1])--
     intersectionpoint(effect, personAfter),
     counterfactualColor, Arrow(TeXHead));
/* individuals */
for(int iperson = 0; iperson < names.length; ++iperson) {
   filldraw(pic, personMarkers[iperson], personColors[iperson]);
}
label(pic, names[0], persons[0], 4*N, namepen, Fill(white));
label(pic, names[1], persons[1], 2*SE, namepen);
label(pic, names[2], persons[2], 3*E, namepen, Fill(white));
label(pic, names[3], persons[3], 5*E, namepen, Fill(white));
/* mark epsilons */
path eps = avgT0--persons[0];
eps = intersectionpoint(avgT0Marker, eps)--intersectionpoint(personMarkers[0], eps);
draw(pic, eps, personColors[0], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
path eps = avgT0--persons[1];
eps = intersectionpoint(avgT0Marker, eps)--intersectionpoint(personMarkers[1], eps);
draw(pic, eps, personColors[1], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
path eps = avgT1--persons[2];
eps = intersectionpoint(avgT1Marker, eps)--intersectionpoint(personMarkers[2], eps);
draw(pic, eps, personColors[2], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.3), W);
path eps = avgT1--persons[3];
eps = intersectionpoint(avgT1Marker, eps)--intersectionpoint(personMarkers[3], eps);
draw(pic, eps, personColors[3], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.3), W);

add(shift(dataOrigin)*pic);


/* ---------- Biased correlational datapoints ---------- */
picture pic = new picture;
/* axes and labels */
path axes = axes();
drawAxes(pic, axes);
label(pic, "Mean independence violated", point(axes, 0), NE);
draw(pic, (educations[0], pais[0]+epss[0])--(educations[0], 0), anglePen + dotted);
draw(pic, (educations[2], pais[2] + epss[2])--(educations[2], 0), anglePen + dotted);
pair biasedOrigin = (5,-8);
pen counterfactualColor = lightgray;
real[] epss = {1.0, -0.8, -0.4, -2.2};  // epsilons
/* compute individuals' location to be plotted later */
path[] personMarkers;
pair[] persons;
for(int iperson = 0; iperson < names.length; ++iperson) {
  pair personBefore = (educations[iperson],
		       pais[iperson] + epss[iperson]);
  persons.push(personBefore);
  path xb = circle(persons[iperson], circleSize);
  personMarkers.push(xb);
}
/* compute midpoints w/o and w/treatment */
pair avgT0 = (persons[0] + persons[1])/2;
pair avgT1 = (persons[2] + persons[3])/2;
draw(pic, avgT0--avgT1, anglePen);
label(pic, "$\E[y|T=0]$", avgT0, 2*W, Fill(white));
label(pic, "$\E[y|T=1]$", avgT1, 4*E, Fill(white));
/* causal effect arrows */
path effect = persons[0]--(persons[0] + deltaEdu*(1,1));
path xa = circle(point(effect, 1), circleSize); 
draw(pic, intersectionpoint(effect, personMarkers[0])--intersectionpoint(effect, xa),
     counterfactualColor, Arrow(TeXHead));
fill(pic, xa, counterfactualColor);
path effect = persons[1]--(persons[1] + deltaEdu*(1,1));
path xa = circle(point(effect, 1), circleSize); 
draw(pic, intersectionpoint(effect, personMarkers[1])--intersectionpoint(effect, xa),
     counterfactualColor, Arrow(TeXHead));
fill(pic, xa, counterfactualColor);
/* average causal effect */
pair cfAvgT0 = (persons[0] + persons[1])/2;
path cf0Marker = circle(cfAvgT0, circleSize);
pair cfAvgT1 = cfAvgT0 + deltaEdu*(1, 1);
path cf1Marker = circle(cfAvgT1, circleSize);
draw(pic, line(cfAvgT0, cfAvgT1, 0, educations[3]+1),
     counterfactualColor + dashed);
/* mark the average causal effect */
real d = 2.5;
path horizontal = cfAvgT0--(cfAvgT1.x + d, cfAvgT0.y);
path vertical = (cfAvgT1.x + d, cfAvgT0.y)--(cfAvgT1) + (d, 0);
draw(pic, horizontal, anglePen);
draw(pic, cfAvgT1--(cfAvgT1 + (d,0)), anglePen);
draw(pic, vertical, anglePen + dashed, Arrow(TeXHead));
label(pic, "$\beta_1$", relpoint(vertical, 0.5), E,
      anglePen, Fill(white));
/* mark correlation effect */
real d = 0.3;
path horizontal = avgT0--(avgT1.x + d, avgT0.y);
path vertical = (avgT1.x + d, avgT0.y)--(avgT1) + (d, 0);
draw(pic, horizontal, anglePen);
draw(pic, avgT1--(avgT1 + (d,0)), anglePen);
draw(pic, vertical, anglePen + dashed, Arrow(TeXHead));
label(pic, "$\alpha_1$", relpoint(vertical, 0.5), 0.05*E,
      anglePen, Fill(white));
/* mark epsilons */
path eps = cfAvgT0--persons[0];
eps = intersectionpoint(cf0Marker, eps)--intersectionpoint(personMarkers[0], eps);
draw(pic, eps, personColors[0], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
eps = cfAvgT0--persons[1];
eps = intersectionpoint(cf0Marker, eps)--intersectionpoint(personMarkers[1], eps);
draw(pic, eps, personColors[1], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
path eps = cfAvgT1--persons[2];
eps = intersectionpoint(cf1Marker, eps)--intersectionpoint(personMarkers[2], eps);
draw(pic, eps, personColors[2], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
real d = 0.4;
draw(pic, cfAvgT1--(cfAvgT1 - d*(1,0)), anglePen);
path horizontal = persons[3]--(persons[3] - d*(1,0));
draw(pic, intersectionpoint(personMarkers[3], horizontal)--point(horizontal, 1),
     anglePen);
path eps = (cfAvgT1 - 0.8*d*(1,0))--(persons[3] - 0.8*d*(1,0));
draw(pic, eps, personColors[3], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
/* plot individuals */
path[] personsMarkers;
for(int iperson = 0; iperson < names.length; ++iperson) {
  path xb = circle(persons[iperson], circleSize);
  personsMarkers.push(xb);
   filldraw(pic, xb, personColors[iperson]);
}
label(pic, names[0], persons[0], 4*N, namepen, Fill(white));
label(pic, names[1], persons[1], 2*SE, namepen);
label(pic, names[2], persons[2], 3*E, namepen, Fill(white));
label(pic, names[3], persons[3], 3*SW, namepen, Fill(white));
/* plot midpoints, actual and counterfactual */
fill(pic, circle(avgT0, circleSize), anglePen);
fill(pic, circle(avgT1, circleSize), anglePen);
fill(pic, cf1Marker, counterfactualColor);

add(shift(biasedOrigin)*pic);
shipout("mean-independence");



/* -------------------------------------------------- */
/* -------------------- causal model 2 -------------- */
/* -------------------------------------------------- */

currentpicture = new picture;
size(130mm,0);
defaultpen(fontsize(8pt));
pen namepen = fontsize(6pt) + gray;

real circleSize = 0.15;
real tickLength = 0.15;

real outcomeOffset = -0.5;  // how much outcome lifter above 0
string[] names = {"Xuande", "Guan Yu", "Zhang Fei", "Cao Cao"};
pen[] personColors = {deepcyan, black, heavygreen, heavyred};
real[] educations = {1, 3, 3, 5};
real deltaEdu = educations[2] - educations[0];
real[] pais = outcomeOffset + educations;
string[] checkers = {"xuande", "zhang-fei", "guan-yu", "cao-cao"};
pen anglePen = gray;
pen counterfactualPen = lightgray;  // for drawing counterfactual markers

void drawAxes(picture pic=currentpicture, path axes) {
  draw(pic, axes, Arrows(TeXHead));
  label(pic, rotate(90)*"Outcome $y$", point(axes, 0), 3*SW);
  label(pic, "Treatment", point(axes, 2), 6*SW);
  draw(pic, (educations[0], 0)--(educations[0], tickLength));
  draw(pic, (educations[2], 0)--(educations[3], tickLength));
  label(pic, "0", (educations[0], 0), S);
  label(pic, "0.5", (educations[1], 0), S);
  label(pic, "1", (educations[3], 0), S);
}


/* ---------- Datapoints according to the correct model ---------- */
picture pic = new picture;
path axes = axes();
pair[] personsBefore, personsAfter;
real[] labelLoc = {0.7, 0.2};  // location of effect labels
real[] epss = {1, -0.9};  // epsilons
real[] ds = {0.9, 0.5};
/* axes and labels */
drawAxes(pic, axes);
label(pic, "Causal effect $\beta_1$", point(axes, 0), NE);
draw(pic, (educations[0], pais[0]+epss[0])--(educations[0], 0), anglePen + dotted);
draw(pic, (educations[0] + deltaEdu, pais[0] + deltaEdu + epss[0])--(educations[0] + deltaEdu, 0), anglePen + dotted);
for(int iperson = 0; iperson < 2; ++iperson) {
  pair personBefore = (educations[iperson],
		       pais[iperson] + epss[iperson]);
  personsBefore.push(personBefore);
  pair personAfter = (educations[iperson] + deltaEdu,
		      pais[iperson] + deltaEdu + epss[iperson]);
  personsAfter.push(personAfter);
  path xb = circle(personBefore, circleSize);
  path xa = circle(personAfter, circleSize);
  path effect = personBefore--personAfter;
  draw(pic, intersectionpoint(xb, effect)--intersectionpoint(xa, effect),
       Arrow(TeXHead));
}
/* add names */
label(pic, names[0], personsBefore[0], 8*N, namepen);
label(pic, names[1], personsBefore[1], 2*SE, namepen);
/* plot persons */
path[] bMarkers, aMarkers;
for(int iperson = 0; iperson < 2; ++iperson) {
  path bMarker = circle(personsBefore[iperson], circleSize);
  bMarkers.push(bMarker);
  filldraw(pic, bMarker, personColors[iperson]);
  add(checkers[iperson], checker(0.3mm, personColors[iperson]));
  path aMarker = circle(personsAfter[iperson], circleSize);
  filldraw(pic, aMarker, pattern(checkers[iperson]));
  aMarkers.push(aMarker);
}

/* mark midpoints w/o and w/treatment */
pair avgT0 = (personsBefore[0] + personsBefore[1])/2;
pair avgT1 = (personsAfter[0] + personsAfter[1])/2;
draw(pic, line(avgT0, avgT1, 0, avgT1.x+1), anglePen + dashed);
// extend the correlation line
path avgT0Marker = circle(avgT0, circleSize);
path avgT1Marker = circle(avgT1, circleSize);
fill(pic, avgT0Marker, anglePen);
add("midpoint", checker(0.3mm, anglePen));
filldraw(pic, avgT1Marker, pattern("midpoint"));
label(pic, "$\E[y|T=0]$", avgT0, 2*W, Fill(white));
label(pic, "$\E[y|T=1]$", avgT1, 4*E, Fill(white));
/* causal effect arrows */
path effect = avgT0--avgT1;
draw(pic, intersectionpoint(effect, avgT0Marker)--intersectionpoint(effect, avgT1Marker),
     anglePen, Arrow(TeXHead));
real d = 0.3;
path horizontal = avgT0--(avgT1.x + d, avgT0.y);
path vertical = (avgT1.x + d, avgT0.y)--(avgT1) + (d, 0);
draw(pic, horizontal, anglePen);
draw(pic, avgT1--(avgT1 + (d,0)), anglePen);
draw(pic, vertical, anglePen + dashed, Arrow(TeXHead));
label(pic, "$\beta_1$", relpoint(vertical, 0.5), E,
      anglePen, Fill(white));
/* mark epsilons */
path eps = avgT0--personsBefore[0];
eps = intersectionpoint(avgT0Marker, eps)--intersectionpoint(bMarkers[0], eps);
draw(pic, eps, personColors[0], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
path eps = avgT0--personsBefore[1];
eps = intersectionpoint(avgT0Marker, eps)--intersectionpoint(bMarkers[1], eps);
draw(pic, eps, personColors[1], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
path eps = avgT1--personsAfter[0];
eps = intersectionpoint(avgT1Marker, eps)--intersectionpoint(aMarkers[0], eps);
draw(pic, eps, personColors[0], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
path eps = avgT1--personsAfter[1];
eps = intersectionpoint(avgT1Marker, eps)--intersectionpoint(aMarkers[1], eps);
draw(pic, eps, personColors[1], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);

add(pic);

/* ---------- correlational datapoints, no bias ---------- */
picture pic = new picture;
pair dataOrigin = (10,0);
pen counterfactualColor = lightgray;
real[] epss = {0.7, -1, 1.1, -1.4};  // epsilons

path axes = axes();
/* axes and markers */
drawAxes(pic, axes);
label(pic, "Mean independence holds", point(axes, 0), NE);
draw(pic, (educations[0], pais[0]+epss[0])--(educations[0], 0), anglePen + dotted);
draw(pic, (educations[2], pais[2] + epss[2])--(educations[2], 0), anglePen + dotted);
path[] personMarkers;
pair[] persons;
for(int iperson = 0; iperson < names.length; ++iperson) {
  pair personBefore = (educations[iperson],
		       pais[iperson] + epss[iperson]);
  persons.push(personBefore);
  path xb = circle(persons[iperson], circleSize);
  personMarkers.push(xb);
}
/* mark midpoints w/o and w/treatment */
pair avgT0 = (persons[0] + persons[1])/2;
pair avgT1 = (persons[2] + persons[3])/2;
draw(pic, avgT0--avgT1, anglePen);
path avgT0Marker = circle(avgT0, circleSize);
path avgT1Marker = circle(avgT1, circleSize);
fill(pic, avgT0Marker, anglePen);
fill(pic, avgT1Marker, anglePen);
label(pic, "$\E[y|T=0]$", avgT0, 2*W, Fill(white));
label(pic, "$\E[y|T=1]$", avgT1, 4*E, Fill(white));
/* mark correlation effect */
real d = 0.3;
path horizontal = avgT0--(avgT1.x + d, avgT0.y);
path vertical = (avgT1.x + d, avgT0.y)--(avgT1) + (d, 0);
draw(pic, horizontal, anglePen);
draw(pic, avgT1--(avgT1 + (d,0)), anglePen);
draw(pic, vertical, anglePen + dashed, Arrow(TeXHead));
label(pic, "$\alpha_1 = \beta_1$", relpoint(vertical, 0.4), E,
      anglePen, Fill(white));
/* mark the causal relationship and the arrows */
path causal = line(avgT0, avgT1, 0, avgT1.x+1);
draw(pic, causal, anglePen + dashed);
path effect = persons[0]--(persons[0] + deltaEdu*(1,1));
path personAfter = circle(point(effect, 1), circleSize);
fill(pic, personAfter, counterfactualColor);
draw(pic, intersectionpoint(effect, personMarkers[0])--
     intersectionpoint(effect, personAfter),
     counterfactualColor, Arrow(TeXHead));
effect = persons[1]--(persons[1] + deltaEdu*(1,1));
personAfter = circle(point(effect, 1), circleSize);
fill(pic, personAfter, counterfactualColor);
draw(pic, intersectionpoint(effect, personMarkers[1])--
     intersectionpoint(effect, personAfter),
     counterfactualColor, Arrow(TeXHead));
/* individuals */
for(int iperson = 0; iperson < names.length; ++iperson) {
   filldraw(pic, personMarkers[iperson], personColors[iperson]);
}
label(pic, names[0], persons[0], 4*N, namepen, Fill(white));
label(pic, names[1], persons[1], 2*SE, namepen);
label(pic, names[2], persons[2], 3*E, namepen, Fill(white));
label(pic, names[3], persons[3], 5*E, namepen, Fill(white));
/* mark epsilons */
path eps = avgT0--persons[0];
eps = intersectionpoint(avgT0Marker, eps)--intersectionpoint(personMarkers[0], eps);
draw(pic, eps, personColors[0], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
path eps = avgT0--persons[1];
eps = intersectionpoint(avgT0Marker, eps)--intersectionpoint(personMarkers[1], eps);
draw(pic, eps, personColors[1], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
path eps = avgT1--persons[2];
eps = intersectionpoint(avgT1Marker, eps)--intersectionpoint(personMarkers[2], eps);
draw(pic, eps, personColors[2], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.3), W);
path eps = avgT1--persons[3];
eps = intersectionpoint(avgT1Marker, eps)--intersectionpoint(personMarkers[3], eps);
draw(pic, eps, personColors[3], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.3), W);

add(shift(dataOrigin)*pic);
shipout("causation-versus-correlation");

/* ---------- Biased correlational datapoints ---------- */
picture pic = new picture;
/* axes and labels */
path axes = axes();
drawAxes(pic, axes);
label(pic, "Mean independence violated", point(axes, 0), NE);
draw(pic, (educations[0], pais[0]+epss[0])--(educations[0], 0), anglePen + dotted);
draw(pic, (educations[2], pais[2] + epss[2])--(educations[2], 0), anglePen + dotted);
pair biasedOrigin = (5,-8);
pen counterfactualColor = lightgray;
real[] epss = {1.0, -0.8, -0.4, -2.2};  // epsilons
/* compute individuals' location to be plotted later */
path[] personMarkers;
pair[] persons;
for(int iperson = 0; iperson < names.length; ++iperson) {
  pair personBefore = (educations[iperson],
		       pais[iperson] + epss[iperson]);
  persons.push(personBefore);
  path xb = circle(persons[iperson], circleSize);
  personMarkers.push(xb);
}
/* compute midpoints w/o and w/treatment */
pair avgT0 = (persons[0] + persons[1])/2;
pair avgT1 = (persons[2] + persons[3])/2;
draw(pic, avgT0--avgT1, anglePen);
label(pic, "$\E[y|T=0]$", avgT0, 2*W, Fill(white));
label(pic, "$\E[y|T=1]$", avgT1, 4*E, Fill(white));
/* causal effect arrows */
path effect = persons[0]--(persons[0] + deltaEdu*(1,1));
path xa = circle(point(effect, 1), circleSize); 
draw(pic, intersectionpoint(effect, personMarkers[0])--intersectionpoint(effect, xa),
     counterfactualColor, Arrow(TeXHead));
fill(pic, xa, counterfactualColor);
path effect = persons[1]--(persons[1] + deltaEdu*(1,1));
path xa = circle(point(effect, 1), circleSize); 
draw(pic, intersectionpoint(effect, personMarkers[1])--intersectionpoint(effect, xa),
     counterfactualColor, Arrow(TeXHead));
fill(pic, xa, counterfactualColor);
/* average causal effect */
pair cfAvgT0 = (persons[0] + persons[1])/2;
path cf0Marker = circle(cfAvgT0, circleSize);
pair cfAvgT1 = cfAvgT0 + deltaEdu*(1, 1);
path cf1Marker = circle(cfAvgT1, circleSize);
draw(pic, line(cfAvgT0, cfAvgT1, 0, educations[3]+1),
     counterfactualColor + dashed);
/* mark the average causal effect */
real d = 2.5;
path horizontal = cfAvgT0--(cfAvgT1.x + d, cfAvgT0.y);
path vertical = (cfAvgT1.x + d, cfAvgT0.y)--(cfAvgT1) + (d, 0);
draw(pic, horizontal, anglePen);
draw(pic, cfAvgT1--(cfAvgT1 + (d,0)), anglePen);
draw(pic, vertical, anglePen + dashed, Arrow(TeXHead));
label(pic, "$\beta_1$", relpoint(vertical, 0.5), E,
      anglePen, Fill(white));
/* mark correlation effect */
real d = 0.3;
path horizontal = avgT0--(avgT1.x + d, avgT0.y);
path vertical = (avgT1.x + d, avgT0.y)--(avgT1) + (d, 0);
draw(pic, horizontal, anglePen);
draw(pic, avgT1--(avgT1 + (d,0)), anglePen);
draw(pic, vertical, anglePen + dashed, Arrow(TeXHead));
label(pic, "$\alpha_1$", relpoint(vertical, 0.5), 0.05*E,
      anglePen, Fill(white));
/* mark epsilons */
path eps = cfAvgT0--persons[0];
eps = intersectionpoint(cf0Marker, eps)--intersectionpoint(personMarkers[0], eps);
draw(pic, eps, personColors[0], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
eps = cfAvgT0--persons[1];
eps = intersectionpoint(cf0Marker, eps)--intersectionpoint(personMarkers[1], eps);
draw(pic, eps, personColors[1], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
path eps = cfAvgT1--persons[2];
eps = intersectionpoint(cf1Marker, eps)--intersectionpoint(personMarkers[2], eps);
draw(pic, eps, personColors[2], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
real d = 0.4;
draw(pic, cfAvgT1--(cfAvgT1 - d*(1,0)), anglePen);
path horizontal = persons[3]--(persons[3] - d*(1,0));
draw(pic, intersectionpoint(personMarkers[3], horizontal)--point(horizontal, 1),
     anglePen);
path eps = (cfAvgT1 - 0.8*d*(1,0))--(persons[3] - 0.8*d*(1,0));
draw(pic, eps, personColors[3], Arrow(TeXHead));
label(pic, "$\epsilon$", relpoint(eps, 0.5), W);
/* plot individuals */
path[] personsMarkers;
for(int iperson = 0; iperson < names.length; ++iperson) {
  path xb = circle(persons[iperson], circleSize);
  personsMarkers.push(xb);
   filldraw(pic, xb, personColors[iperson]);
}
label(pic, names[0], persons[0], 4*N, namepen, Fill(white));
label(pic, names[1], persons[1], 2*SE, namepen);
label(pic, names[2], persons[2], 3*E, namepen, Fill(white));
label(pic, names[3], persons[3], 3*SW, namepen, Fill(white));
/* plot midpoints, actual and counterfactual */
fill(pic, circle(avgT0, circleSize), anglePen);
fill(pic, circle(avgT1, circleSize), anglePen);
fill(pic, cf1Marker, counterfactualColor);

add(shift(biasedOrigin)*pic);
shipout("causal-model-2");



/* ----------------------------------------------------------------------
   -------------------- causation versus correlation --------------------
   ---------------------------------------------------------------------- */

currentpicture = new picture;
size(130mm,0);
defaultpen(fontsize(8pt));


real circleSize = 0.2;

real[] educations = {1, 4};
real[] pais = educations;
string[] names = {"Xuande", "Zhang Fei"};

path axes(pair origin = (0,0)) {
  path a = (origin.x, origin.y+5)--origin--(origin.x+6, origin.y);
  return a;
}

void drawAxes(picture pic=currentpicture, path axes) {
  draw(pic, axes, Arrows(TeXHead));
  label(pic, rotate(90)*"pay", point(axes, 0), 3*SW);
  label(pic, "education", point(axes, 2), 2*SW);
  pair origin = point(axes, 1);
  real l = min(point(axes, 0).y - origin.y, point(axes, 2).x - origin.x);
  // length of the shortest axis, needed for the diagonal line
  draw(pic, origin--(origin + 0.9*l*(1,1)), gray+dashed);
}

/* ---------- Left: same person with and without education ---------- */
picture pic = new picture;
path causalAxes = axes();
drawAxes(pic, causalAxes);
int iperson = 0;
real deltaEdu = 3;
pair personBefore = (educations[iperson], pais[iperson]);
pair personAfter = (educations[iperson] + deltaEdu, pais[iperson] + deltaEdu);
path xb = circle(personBefore, circleSize);
path xa = circle(personAfter, circleSize);
path effect = personBefore--personAfter;
filldraw(pic, xb, personColors[iperson]);
add("checker", checker(0.5mm, personColors[iperson]));
filldraw(pic, xa, pattern("checker"));
draw(pic, intersectionpoint(xb, effect)--intersectionpoint(xa, effect),
     Arrow(TeXHead));
draw(pic, (personBefore.x, personBefore.y-circleSize)--
     (personBefore.x, -circleSize),
     dotted);
draw(pic, (personBefore.x - circleSize, personBefore.y)--
     (-circleSize, personBefore.y),
     dotted);
draw(pic, (personAfter.x, personAfter.y-circleSize)--
     (personAfter.x, -circleSize),
     dotted);
draw(pic, (personAfter.x - circleSize, personAfter.y)--
     (-circleSize, personAfter.y),
     dotted);
/* mark the effect size angle */
path horizontal = personBefore--(personBefore + (deltaEdu, 0));
draw(pic, intersectionpoint(horizontal, xb)--point(horizontal, 1), anglePen);
path angleArc = arc(personBefore, 2*circleSize, 0.0, 45.0);
draw(pic, angleArc, anglePen);
label(pic, "$\beta_1$: true effect of education", personBefore,
      6*dir(20), anglePen, Fill(white));
/* Education/pay changes */
path eduChange = (personBefore.x, -circleSize)--
  (personAfter.x, -circleSize);
draw(pic, eduChange, Arrow(TeXHead));
label(pic, "More education", relpoint(eduChange, 0.5), S);
path payChange = (-circleSize, personBefore.y)--
  (-circleSize, personAfter.y);
draw(pic, payChange, Arrow(TeXHead));
label(pic, rotate(90)*"More pay", relpoint(payChange, 0.5), W);
/* Labels for persons */
label(pic, names[iperson] + " without college degree", personBefore, 3*SE,
      Fill(white));
label(pic, names[iperson] + " with college degree", personAfter, 4*dir(120),
      Fill(white));

add(pic);


picture pic = new picture;
drawAxes(pic, axes());
int iperson1 = 0;
int iperson2 = 1;
pair person1 = (educations[iperson1], pais[iperson1]);
pair person2 = (educations[iperson2], pais[iperson2]);
path xb = circle(person1, circleSize);
path xa = circle(person2, circleSize);
path effect = person1--person2;
filldraw(pic, xb, personColors[iperson1]);
filldraw(pic, xa, personColors[iperson2]);
draw(pic, (person1.x, person1.y-circleSize)--
     (person1.x, -circleSize),
     dotted);
draw(pic, (person1.x - circleSize, person1.y)--
     (-circleSize, person1.y),
     dotted);
draw(pic, (person2.x, person2.y-circleSize)--
     (person2.x, -circleSize),
     dotted);
draw(pic, (person2.x - circleSize, person2.y)--
     (-circleSize, person2.y),
     dotted);
/* mark the effect size angle */
path horizontal = person1--(person2.x, person1.y);
draw(pic, intersectionpoint(horizontal, xb)--point(horizontal, 1), anglePen);
path angleArc = arc(person1, 2*circleSize, 0.0, 45.0);
draw(pic, angleArc, anglePen);
/* Education/pay changes */
path eduChange = (personBefore.x, -circleSize)--
  (personAfter.x, -circleSize);
draw(pic, eduChange, dotted);
label(pic, "More education", relpoint(eduChange, 0.5), S);
path payChange = (-circleSize, personBefore.y)--
  (-circleSize, personAfter.y);
draw(pic, payChange, dotted);
label(pic, rotate(90)*"More pay", relpoint(payChange, 0.5), W);
/* Labels for persons */
label(pic, names[iperson1] + " without college degree", person1, 3*SE,
      Fill(white));
label(pic, names[iperson2] + " with college degree", person2, 4*dir(120),
      Fill(white));
label(pic, "$\alpha_1$: correlational effect", person1,
      6*dir(20), anglePen, Fill(white));

add(shift((7,0))*pic);
shipout("causation-vs-correlation");
