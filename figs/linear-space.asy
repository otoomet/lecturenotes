usepackage("amsbsy");
unitsize(8mm);
defaultpen(fontsize(8));

pair a = (3, 2);
pair b = (5, -2);
pair c = (8, 3);
pair d = (-3, 0.2);

dot((0,0));
draw((0,0)--a, mediumred, Arrow);
label("$\boldsymbol{a}$", a, NW);
draw((0,0)--b, mediumblue, Arrow);
label("$\boldsymbol{b}$", b, SW);
draw((0,0)--c, Arrow);
draw((0,0)--d, Arrow);

// find the coefficients for c
real det = cross(a, b);
real alpha = cross(c, b)/det;
real beta = cross(a, c)/det;
// draw the fitted lines
draw((0,0)--alpha*a--(alpha*a + beta*b), dotted+lightred);
dot(alpha*a, lightred);
label(format("%.2f", alpha) + "$\boldsymbol{a}$", alpha*a, N);
draw((0,0)--beta*b--(beta*b + alpha*a), dotted+lightblue);
dot(beta*b, lightblue);
label(format("%.2f", beta) + "$\boldsymbol{b}$", beta*b, SW);
label("$\boldsymbol{c} = " +
      format("%.2f", alpha) + "\boldsymbol{a} + " +
      format("%.2f", beta) + "\boldsymbol{b}$", c, SE);

// find the coefficients for d
real det = cross(a, b);
real alpha = cross(d, b)/det;
real beta = cross(a, d)/det;
// draw the fitted lines
draw((0,0)--alpha*a--(alpha*a + beta*b), dotted+lightred);
dot(alpha*a, lightred);
label(format("%.2f", alpha) + "$\boldsymbol{a}$", alpha*a, N);
draw((0,0)--beta*b--(beta*b + alpha*a), dotted+lightblue);
dot(beta*b, lightblue);
label(format("%.2f", beta) + "$\boldsymbol{b}$", beta*b, SW);
label("$\boldsymbol{d} = " +
      format("%.2f", alpha) + "\boldsymbol{a} + " +
      format("(%.2f", beta) + "\boldsymbol{b})$", d, SW);
