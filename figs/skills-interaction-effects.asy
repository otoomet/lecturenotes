unitsize(70mm,0.7mm);
defaultpen(fontsize(8));

real xlevel=30;  // height of x-axis
path xaxis = (-0.1,xlevel)--(1.1,xlevel);
draw(xaxis, ArcArrow(SimpleHead));
label("Cognitive", point(xaxis, 1), NE);
label("skills", point(xaxis, 1), SE);
path yaxis = (0,xlevel-2)--(0,105);
draw(yaxis, ArcArrow(SimpleHead));
label("Income (\$1000)", point(yaxis, 1), E);

real low = 0.22;
real high = 0.9;
real b0 = 40;
real bs = 10;
real bc = 20;
real bsc = 30;

path lowSocial=(low,b0)--(high,b0+bc);
draw(lowSocial, blue, MarkFill[0]);
label("Low social skills", relpoint(lowSocial, 0.25), SE);
path highSocial=(low,b0+bs)--(high,b0+bs+bc+bsc);
draw(highSocial, red, MarkFill[0]);
label("High social skills", relpoint(highSocial, 0.5), NW);
draw((low,xlevel)--point(highSocial, 0), dotted);
label("Low", (low,xlevel), S);
draw((high,xlevel)--point(highSocial, 1), dotted);
label("High", (high,xlevel), S);

draw((0,b0)--(high,b0), dotted);
label("$\beta_0 = 40$", (0,b0), W);
draw((0,b0+bs)--(high,b0+bs), dotted);
label("$\beta_0 + \beta_s = 50$", (0,b0 + bs), W);
draw((0,b0+bc)--(high,b0+bc), dotted);
label("$\beta_0 + \beta_c = 60$", (0,b0 + bc), W);
draw((0,b0+bs+bc+bsc)--(high,b0+bs+bc+bsc), dotted);
label("$\beta_0 + \beta_s + \beta_c + \beta_sc = 100$", (0,b0 +bs+ bc+bsc), W);

path bsEffect = (low, b0)--(low, b0+bs);
draw(bsEffect, ArcArrows(SimpleHead));
label("$\beta_s = 10$", relpoint(bsEffect, 0.5), W);
path bsEffect1 = (high, b0+bc)--(high, b0+bc+bs);
draw(bsEffect1, ArcArrows(SimpleHead));
label("$\beta_s = 10$", relpoint(bsEffect1, 0.5), E);
draw(shift((0,bs))*lowSocial, blue+dotted);
path bcEffect = (high, b0)--(high, b0 + bc);
draw(bcEffect, ArcArrows(SimpleHead));
label("$\beta_c = 20$", relpoint(bcEffect, 0.5), E);
path bscEffect = (high, b0+bs+bc)--(high, b0 +bs+bc+bsc);
draw(bscEffect, ArcArrows(SimpleHead));
label("$\beta_{sc} = 30$", relpoint(bscEffect, 0.5), E);

