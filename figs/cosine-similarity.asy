usepackage("amsbsy");
unitsize(20mm);
defaultpen(fontsize(10));

real tick = 0.1;
path xaxis = (-0.5,0)--(2.5,0);
path yaxis = (0,-1.2)--(0, 2.5);
draw(xaxis, Arrow(HookHead));
draw(yaxis, Arrow(HookHead));
label("$x$", relpoint(xaxis, 1), S);
label("$y$", relpoint(yaxis, 1), SW);

// vectors
path a = (0,0)--(1,1);
path b = (0,0)--(2,2);
path c = (0,0)--(2,-1);
draw(a, Arrow(TeXHead));
draw(b, Arrow(TeXHead));
draw(c, Arrow(TeXHead));
label("$\boldsymbol{x}_1$", relpoint(a, 1), SE);
label("$\boldsymbol{x}_2$", relpoint(c, 1), SW);
label("$\boldsymbol{x}_3$", relpoint(b, 1), SE);

// coordinate ticks
for(real i = 1; i < relpoint(xaxis, 1).x; ++i) {
  draw((i,0)--(i,-tick));
  label(string(i), (i,-tick), S);
}
for(real i = 1; i < relpoint(yaxis, 1).y; ++i) {
  draw((0,i)--(-tick,i));
  label(string(i), (-tick, i), W);
}

// vector endpoints
path[] vectors = {a, b, c};
for(path v : vectors) {
  draw((0,relpoint(v, 1).y)--relpoint(v,1)--(relpoint(v,1).x, 0), dotted);
}

// angles
path ar1 = arc((0,0), 0.3, 0, 45);
real an2 = atan2(relpoint(c, 1).y, relpoint(c, 2).x);  // angle
an2 = round(degrees(an2));
path ar2 = arc((0,0), 0.3, 0, an2);
draw(ar1);
draw(ar2);
label("$45^\circ$", relpoint(ar1, 0.5), NE);
label("$" + string(an2) + "^\circ$", relpoint(ar2, 0.5), SE);
