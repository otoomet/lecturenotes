// Demo of 1-d k-means clusters and loss function
defaultpen(fontsize(8pt));
texpreamble("\input{../isomath}");
usepackage("amsbsy");

real figwidth=120mm;
size(figwidth,0);

/* data points */
real size[] = {180, 200, 200, 400, 400, 420};
real crime[] = {10, 10, 20, 20, 10, 10};

int membership1[] = {1,1,1, 2,2,2};
int membership2[] = {1,1,2, 2,1,1};
real r = 0.05;
pen color[] = {black, lightred};

real ytick = 10;
real xtick = 50;
real ticklength=0.1;
real distanceshift=3*r;

void drawData(real yscale, int[] membership) {
  /* x-axis */
  path axes = (0, yscale*max(crime)*1.05)--(0,0)--(max(size)*1.05, 0);
  draw(axes, Arrows);
  label("crime rate, per 1000", point(axes, 0), 2*SW);
  label("$0$", point(axes, 1), SW);
  label("size, $m^2$", point(axes, 2), 2*SW);
  /* ticks */
  for(real y = 0; y < point(axes, 0).y; y += ytick) {
    draw((0, y)--(-ticklength, y));
  }
  for(real x = 0; x < point(axes, 2).x; x += xtick) {
    draw((x, 0)--(x, -ticklength));
  }
  /* data points */
  for(int i = 0; i < size.length; ++i) {
    int cluster = membership[i];
    real x = size[i];
    real y = yscale*crime[i];
    fill(circle((x, y), r), color[cluster - 1]);
    draw(circle((x, y), r));
    /* labels */
    label("$x_"+string(i+1)+"$", (x, y+r), N);
  }
}

/* Good memberships */
drawData(10, membership1);

/* distances */
path d12 = (size[0],crime[0]-distanceshift)--(size[1], crime[0]-distanceshift);
draw(d12, Arrows(TeXHead));
label("$d_{12}=" + string(arclength(d12)) + "$",
      relpoint(d12, 0.5), N);

// drawData(10.0, membership2);
// shipout("clustering-distance-scale-1000.pdf");
