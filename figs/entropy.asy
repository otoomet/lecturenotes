unitsize(24mm);
usepackage("bbm");

real bw=0.85;  // box width

int c = 1;  // how many boxes full
int ey = 0; // vertical positio
int row = 1;  // row number

string[] states = {"$A$", "$B$", "$C$", "$D$", "$E$", "$F$", "$G$", "$H$"};
  
void row(int c) {
  real P = 1/c;
  pen col = gray(1 - P);
  real H = 0;
  label(string(row), (bw, ey + bw/2), W, fontsize(14));
  for(int i = 1; i <= 8; ++i) {
    path b = box((i, bw + ey), (i+bw, ey));
    draw(b);
    if(i <= c) {
      fill(b, col);
      label("$P=" + string(P, 3) + "$", point(b, 0), SE, Fill(white));
      H -= P*log(P)/log(2);
    }
  }
  label("$\mathbbm{H}=" + string(H, 3) + "$", (9, ey + bw/2), E,
	fontsize(14));
  --ey;
  ++row;
}

// create state labels
for(int i = 0; i < 8; ++i) {
  pair s = (i + 1 + bw/2, row);
  label(states[i], s, fontsize(14));
}

row(1);
row(2);
row(3);
row(4);
row(5);
row(6);
row(7);
row(8);
