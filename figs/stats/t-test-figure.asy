usepackage("amsbsy");
size(130mm);
defaultpen(fontsize(9));

real tick = 0.002;  // tick length
real ci = 0.5; // CI boundary height
real sigma = 0.03;
real H0 = 0.9;  // H0: 90% in time
real zcr = 1.96;
real phat = 0.82;  // proportion in data

real labelHeight = 2;
/* mark normal curve and gray it out */
real dnorm(real x, real mu=H0, real sigma=sigma) {
  real d = 1/sqrt(2*pi)/sigma*exp(-0.5*((x - mu)/sigma)^2);
  return 0.005*d;  // return scaled version
}
real exMin = 0.79;  // minimum axis value
real exMax = 1.01;
guide normal = (exMax, dnorm(exMax))--(exMax, 0)--
  (exMin, 0)--(exMin, dnorm(exMin));
for(real x = 0.8; x < 1.0; x += 0.01) {
  normal = normal..(x, dnorm(x));
}
normal = normal--cycle;
draw(normal, gray(0.8));
fill(normal, palecyan);
label("Simulated results", (H0, dnorm(H0)), 3*S, royalblue);

path xaxis = (exMin, 0)--(exMax, 0);
draw(xaxis, Arrow(HookHead));
label("Sample average", point(xaxis, 1), 2*NW);
real[] ticks = {0.8, 0.9, 1.0};
for(real tick: ticks) {
  draw((tick, 0)--(tick,dnorm(H0)), dotted);
  label(string(tick), (tick, 0), labelHeight*S);
}

real distanceHeight = 0.006;
/* mark and label 95% CI */
/* mark ticks on the low side */
real CIHeight = 4*distanceHeight;
real[] ticks = {H0 - zcr*sigma, H0, H0 + zcr*sigma, phat};
for(real loc: ticks) {
  draw((loc,0)--(loc,-tick));
}
/* mark 95% CI on the upper side */
real[] cibounds = {H0 - zcr*sigma, H0 + zcr*sigma};
for(real loc: cibounds) {
  draw((loc,0)--(loc, (CIHeight + tick)),
       orange);
}
draw((cibounds[0], CIHeight)--(cibounds[1], CIHeight),
     Arrows(TeXHead));
label("95\% of simulations under $H_0$ are here", (H0, CIHeight), N);
/* Mark data */
draw((phat,0)--(phat,CIHeight));

/* mark distance */
draw((H0,0)--(H0,distanceHeight));
draw((H0, distanceHeight)--(phat, distanceHeight),
     Arrows(TeXHead));
label("distance between $H_0$ and data = " + string(H0 - phat, 2),
      (H0 + (phat - H0)/2, distanceHeight), 2*N,
      Fill(white));

/* labels */
real textHeight = 7;
label("$H_0: p =" + string(H0) + "$", (H0, 0), textHeight*S);
label("$p - z_{cr}\cdot\sigma$", (H0 - zcr*sigma, 0), 1.6*S);
label("$" + string(H0 - zcr*sigma, 2) + "$", ((cibounds[0], CIHeight)), SW);
label("$p + z_{cr}\cdot\sigma$", (H0 + zcr*sigma, 0), 1.6*S);
label("$" + string(H0 + zcr*sigma, 2) + "$", ((cibounds[1], CIHeight)), SE);
label(string(phat), (phat, 0), labelHeight*S);
label("$\hat p =" + string(phat) + "$ in data", (phat, 0), textHeight*S);
