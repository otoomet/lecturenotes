/* explain quantile */
import patterns;
texpreamble("\input{../tex/isomath}");

defaultpen(fontsize(9));
size(110mm, 0);
pen bgcolor = palegray;
picture pic = new picture;

real datapoints[] = {-2, 1, 1, 3};
int n = datapoints.length;

/* axis */
path axis = (0, 0)--(n-1, 0);
draw(pic, axis);
real qheight = -0.1;
real dheight = -qheight;
label(pic, "Ordered datapoints", (0, dheight), 4*WNW);
label(pic, "Quantiles", (0, qheight), 4*WSW);

/* data quantiles */
dotfactor = 10;
real quantiles[];
for(int i = 0; i < n; ++i) {
  quantiles[i] = i/(n - 1);
  dot(pic, (i, 0));
  /* data point values */
  draw(pic, (i, 0)--(i, dheight), dotted);
  label(pic, string(datapoints[i]), (i, dheight), N);
  /* lines corresponding to datapoint quantiles */
  draw(pic, (i, 0)--(i, qheight),
       dotted);
  label(pic, string(quantiles[i], 3), (i, qheight), S);
}

/* mark the sample quantiles */
real sqheight = 3*qheight;
real newQuantiles[] = { 0.25, 0.5 }; // quantiles not in data
for(real quantile : newQuantiles) {
  real location = quantile*(n - 1);
  draw(pic, (location, sqheight)--(location, 0), dotted,
       Arrow(TeXHead));
  label(pic, string(quantile, 3),
	(location, sqheight), S);
}

add(pic, Fill(bgcolor));
