usepackage("amsbsy");
size(100mm,0);
defaultpen(fontsize(8pt));
texpreamble("\input{../isomath}");

// Filters and image
real F[][] = {{-1, 1},
	      {-1, 1}};
real Img[][] = {{0, 0, 0, 0},
		{0, 0, 1, 1},
		{0, 0, 1, 1}};
real Result[][] = {{0, 1, 0},
		   {0, 2, 0}};

real matScale = 0.4;
pair matLoc = (-5*matScale, 0);
// loop: vertical first
for(int i = 0; i < Img[1].length; ++i) {
  for(int j = 0; j < Img.length; ++j) {
    real col = Img[j][i];
    pair bl = matLoc + (i, j)*matScale;
    pair tr = bl + (matScale, matScale);
    if(col > 0.5) {
      fill(box(bl, tr));
    } else {
      draw(box(bl, tr));
    }
    label(format(col), midpoint(bl--tr), Fill(white));
  }
}
// draw filter
pair filterLoc = (-4*matScale, -4*matScale);
// loop: vertical first
for(int i = 0; i < F[1].length; ++i) {
  for(int j = 0; j < F.length; ++j) {
    real col = F[j][i];
    pair bl = filterLoc + (i, j)*matScale;
    pair tr = bl + (matScale, matScale);
    draw(box(bl, tr));
    label(format(col), midpoint(bl--tr), Fill(white));
  }
}
// draw result
pen[] col = {rgb("#66C2A5"), rgb("#FC8D62"), rgb("#8DA0CB")};
pair resultLoc = (7*matScale, -3*matScale);
real delta = 0.05;
// loop: vertical first
for(int i = 0; i < Result[1].length; ++i) {
  for(int j = 0; j < Result.length; ++j) {
    real pixelValue = Result[j][i];
    pair bl = resultLoc + (i, j)*matScale;
    pair tr = bl + (matScale, matScale);
    draw(box(bl, tr));
    // if first row, mark the window positions
    if(j == 1) {
      draw(box(bl + delta*(1,1), tr - delta*(1,1)), col[i] + linewidth(1));
    }
    label(format(pixelValue), midpoint(bl--tr), Fill(white));
  }
}

// convolution frames
pair[] expressionLoc = {(1*matScale, Img.length*matScale),
		    (3*matScale, (Img.length - 0.7)*matScale),
		    (5*matScale, (Img.length - 1.4)*matScale)};
string[] result = {"$(-1\cdot0) + (-1\cdot0) + (1\cdot0) + (1\cdot0) = 0$",
		   "$(-1\cdot0) + (-1\cdot0) + (1\cdot1) + (1\cdot1) = 2$",
		   "$(-1\cdot1) + (-1\cdot1) + (1\cdot1) + (1\cdot1) = 0$"
};
pair[] framedelta = {(0.1, -0.1), (0.1, 0.1), (0.1, -0.2)};
int j = 1;
pair filterR = filterLoc + (F[1].length, F.length/2)*matScale;
pair filterT = filterLoc + (F[1].length/2, F.length)*matScale;
// move window over image
for(int window = 0; window < 3; ++window) {
  pair bl = matLoc + (window*matScale, j*matScale) + framedelta[window]*matScale;
  pair tr = bl + (2*matScale, 2*matScale);
  guide windowFrame = box(bl, tr);
  draw(windowFrame, col[window]);
  draw(point(windowFrame, 2.5){up}..{right}expressionLoc[window], col[window], Arrow);
  draw(filterR{right}..expressionLoc[window] + (1*matScale, -0.4*matScale), col[window], Arrow);
  draw(filterT--point(windowFrame, 0.5), col[window], Arrow);
  label(result[window], expressionLoc[window], E);
}
// matrix labels, do these here in order to overlap the arrows
label("Image", matLoc + (Img[1].length/2*matScale, (Img.length + 0.5)*matScale), Fill(white));
label("Filter", filterLoc + (F[1].length/2*matScale, (F.length + 0.5)*matScale), Fill(white));
label("Result", resultLoc + (Result[1].length/2*matScale, (Result.length + 0.5)*matScale), Fill(white));
