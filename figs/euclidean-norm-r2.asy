usepackage("amsbsy");
unitsize(50mm);
defaultpen(fontsize(10));

real tick = 0.05;
real tickStep = 0.25;
path xaxis = (-0.1,0)--(1.2,0);
path yaxis = (0,-0.2)--(0, 1.2);
draw(xaxis, Arrow(HookHead));
draw(yaxis, Arrow(HookHead));
label("$x$", relpoint(xaxis, 1), S);
label("$y$", relpoint(yaxis, 1), SW);
label("0", (0,0), SW);

// vectors
path a = (0,0)--(1,1);
draw(a, Arrow(TeXHead));
label("$\boldsymbol{v} = (1,1)$", relpoint(a, 1), SE);

// coordinate ticks
for(real i = tickStep; i < relpoint(xaxis, 1).x; i += 0.25) {
  draw((i,0)--(i,-tick));
  if(i % 1 == 0) {
    label(string(i), (i,-tick), S);
  }
}
for(real i = tickStep; i < relpoint(yaxis, 1).y; i += 0.25) {
  draw((0,i)--(-tick,i));
  if(i % 1 == 0) {
    label(string(i), (-tick, i), W);
  }
}

// vector endpoints
path[] vectors = {a};
for(path v : vectors) {
  draw((0,relpoint(v, 1).y)--relpoint(v,1)--(relpoint(v,1).x, 0), dotted);
}

label("Length = $\sqrt{2}$", relpoint(a, 0.5), SE);
