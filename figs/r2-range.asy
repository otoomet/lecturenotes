/* */
import patterns;
texpreamble("\input{../isomath}");

defaultpen(fontsize(8));
pen bgcolor = palegray;
pen[] pointColors = {rgb("#F8766D"), rgb("#7CAE00"),
		     rgb("#00BFC4"), rgb("#C77CFF")};
pair[] datapoints = {(1,1.5), (2,0.5), (3,4.5), (4,3.5)};
// this gives beta0 = 0, beta1 = 1
pair[] dpLabelDir = {2*NE, 2*E, 2*SE, 2*S};
int imin = 1;
int imax = 2;
real beta0 = 0;
real beta1 = 1;

real figwidth=75mm;
picture pic = new picture;
add(pic, Fill(bgcolor));
size(figwidth,0);

/* axes and such */
real extendDelta = 0.2;  // how much to draw lines over axis line
real ticklength = 0.1;
path axes = (0,5.3)--(0,0)--(5,0);
draw(pic, axes, Arrows(TeXHead));
for(int x = 1; x < point(axes, 2).x; ++x) {
  draw(pic, (x, 0)--(x,-ticklength));
}
for(int y = 1; y < point(axes, 0).y; ++y) {
  draw(pic, (0, y)--(-ticklength, y));
}
label(pic, "$y$", point(axes, 0), SE);
label(pic, "$x$", point(axes, 2), NW);

/* datapoints */
dotfactor=11;
for(int i = 0; i < datapoints.length; ++i) {
  dot(pic, datapoints[i], pointColors[i]);
  label(pic, "$\vec{x}_" + string(i+1) +"$",
	datapoints[i], dpLabelDir[i]);
}

/* Regression line */
real xStart = 0.5;
real xEnd = point(axes, 2).x - 0.5;
path regLine = (xStart,beta0 + beta1*xStart)--(xEnd, beta0 + beta1*xEnd);
draw(pic, regLine, linewidth(1));

/* Range of y */
path urange = datapoints[2]--(-extendDelta, datapoints[2].y);
draw(pic, urange, pointColors[2] + dotted + linewidth(1));  // upper total
path lrange = datapoints[1]--(-extendDelta, datapoints[1].y);
draw(pic, lrange, pointColors[1] + dotted + linewidth(1));  // lower total
path totalRange = point(urange, 1)--point(lrange, 1);
draw(pic, totalRange, Arrows(TeXHead));
label(pic, rotate(90)*("Total range in data $\mathit{TR} = " +
		       string(arclength(totalRange)) +
		       "$"),
      relpoint(totalRange, 0.5), W);

/* Residuals */
path[] e;
for(int i = 0; i < datapoints.length; ++i) {
  real x = datapoints[i].x;
  real yhat = beta0 + beta1*x;
  e[i] = datapoints[i]--(x, yhat);
  draw(pic, e[i], pointColors[i] + dotted);
  label(pic, "$e_" + string(i) + "$ = " + string(arclength(e[i])),
	relpoint(e[i], 0.5), E, Fill(bgcolor));
}

/* leftover range */
path leftoverU = shift(-datapoints[imax].x + extendDelta,0)*e[imax];
path leftoverL = shift(-datapoints[imin].x + extendDelta,
		       point(leftoverU, 1).y - point(e[imin], 1).y)*e[imin];
draw(pic, leftoverU, pointColors[imax], Arrows(TeXHead));
draw(pic, leftoverL, pointColors[imin], Arrows(TeXHead));
label(pic, rotate(90)*("$e_" + string(imax) + "$ = " + string(arclength(leftoverU))),
      relpoint(leftoverU, 0.5), E, Fill(bgcolor));
label(pic, rotate(90)*("$e_" + string(imin) + "$ = " + string(arclength(leftoverL))),
      relpoint(leftoverL, 0.5), E, Fill(bgcolor));
/* connect ranges to leftover range */
draw(pic, point(e[imax], 1)--point(leftoverU, 1), dotted);  // upper-range-lower connector
draw(pic, point(e[imin], 0)--point(leftoverL, 0), dotted);  // lower-range
draw(pic, point(e[imin], 1)--point(leftoverL, 1), dotted);
path leftoverRange = shift(2*extendDelta, 0)*(point(leftoverL, 0)--point(leftoverU, 0));
draw(pic, leftoverRange, Arrows(TeXHead));
label(pic, rotate(90)*("Error range $\mathit{ER} = " +
		       string(arclength(leftoverRange)) +
		       "$"),
      relpoint(leftoverRange, 0.5), E,
      Fill(bgcolor));
/* connect leftover range marker with upper/lower leftover ranges */
draw(pic, point(leftoverRange, 0)--point(leftoverL, 0), dotted);

add(pic, Fill(bgcolor));
