SOURCES = machineLearning.rnw
ASY_FIGURES = $(wildcard figs/*.asy)
RMDS = $(wildcard */*.rmd)
PDFS = machineLearning.pdf
HTMLS = machinelearning-py.html machinelearning-R.html
PDF_FIGURES = $(patsubst %.asy, %.pdf, $(ASY_FIGURES))
PDF_FIGURES := $(PDF_FIGURES) 1d-k-means-bad.pdf 1d-k-means-good.pdf\
 2d-movie-ratings-centered.pdf 2d-movie-ratings-standard.pdf\
 multi-layer-perceptron.pdf

## run in parallel using 50% of available threads
PARALLEL := $(shell expr $(shell nproc) / 2 )
$(info using $(PARALLEL)-fold parallelism)
MAKEFLAGS += -j$(PARALLEL)

# rsync arguments
RSYNC = rsync -rlptoDvuK --delete
OVID = $(HOME)/www/ovid

$(info HTMLS: $(HTMLS))
$(info SOURCES: $(SOURCES))
$(info PDFS: $(PDFS))
$(info ASY_FIGURES: $(ASY_FIGURES))
$(info PDF_FIGURES: $(PDF_FIGURES))

PROJECTDIR = $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

all: $(PDFS) $(HTMLS)
	echo "All done"

%.tex: %.rnw scripts/tools.R
	Rscript -e "knitr::knit('$<', quiet=FALSE)"

# for debugging
.PRECIOUS: machineLearning.tex

%.pdf: export TEXINPUTS := ${TEXINPUTS}:./figs  # requires v4.59+
machineLearning.pdf: machineLearning.tex \
 solutions.tex intro-to-stats.tex \
 $(PDF_FIGURES) tex/lecturenotes.bib tex/isomath.tex\
 figs/smoking-cancer.pdf
	latexmk -f -xelatex -interaction=nonstopmode $< &&\
	latexmk -c
# -f: force continued processing in case of errors
# -c: delete intermediates

machinelearning-py.html:
	$(MAKE) -C machinelearning-py

.PHONY: machinelearning-R.html
machinelearning-R.html:
	$(MAKE) -C machinelearning-R

.PHONY: datascience-intro.html
datascience-intro.html:
	$(MAKE) -C datascience-intro

# asymptote figures in figs/
# figs: phony target, ensure subfolder make is run just once
$(PDF_FIGURES): figs
.PHONY: figs
figs:
	$(MAKE) -C figs

# publish the built book on ovid: faculty.washington.edu/otoomet/info201-book
# first copy to $HOME/www/ovid, thereafter the whole ovid to the ovid server
publish:
	$(RSYNC) machineLearning.pdf $(OVID)/ && \
	$(RSYNC) machinelearning-py/build/ $(OVID)/machinelearning-py && \
	$(RSYNC) machinelearning-py/figs $(OVID)/machinelearning-py/figs && \
	$(RSYNC) machinelearning-R/build/ $(OVID)/machinelearning-R && \
	$(RSYNC) machinelearning-R/figs $(OVID)/machinelearning-R/figs && \
	$(RSYNC) datascience-intro/build/ $(OVID)/datascience-intro && \
	$(RSYNC) datascience-intro/figs $(OVID)/datascience-intro/figs && \
	$(RSYNC) datascience-intro/img $(OVID)/datascience-intro/img && \
	$(RSYNC) files $(OVID) &&\
	$(RSYNC) $(OVID)/ ovid:public_html/

clean: clean-top clean-py clean-R clean-figs

.PHONY: clean-top
clean-top:
	rm -fv *.aux *.log *.nav *.out *.snm *.toc *~ *.vrb *.bbl *.dvi *.blg *.xdv\
               *.bbl *.chs

.PHONY: clean-py
clean-py:
	$(MAKE) -C machinelearning-py clean

.PHONY: clean-R
clean-R:
	$(MAKE) -C machinelearning-R clean

.PHONY: clean-figs
clean-figs:
	$(MAKE) -C figs clean

