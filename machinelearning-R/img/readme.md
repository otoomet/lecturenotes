# img

Images where the displayed version (png, jpg, ...) is the original
versioned version.

All images are public domain, unless listed otherwise.

## datasets

* **iris-virginica**: 
  Source: Eric Hunt,
  [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), via
  [Wikimedia
  Commons](https://commons.wikimedia.org/wiki/File:Iris_virginica_2.jpg).
