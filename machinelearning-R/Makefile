HTML = build/index.html
RMDS = $(wildcard *.rmd)
MDS = $(patsubst %.rmd, %.md, $(RMDS))
XOPPS = $(wildcard figs/*/*.xopp)
PDF_PAGES = $(patsubst %.xopp, %_page.pdf, $(XOPPS))
PDF_FIGURES = $(patsubst %_page.pdf, %.pdf, $(PDF_PAGES))
PNG_FIGURES = $(patsubst %.pdf, %.png, $(PDF_FIGURES))
PROJECTDIR = $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

## run in parallel using 50% of available threads
PARALLEL := $(shell expr $(shell nproc) / 2 )
$(info using $(PARALLEL)-fold parallelism)
MAKEFLAGS += -j$(PARALLEL)

all: $(HTML)

%.md: %.rmd
	Rscript -e "knitr::knit('$<', quiet=FALSE)"

web-scraping.md: ../machinelearning-common/web-scraping-ethics-html.rmd\
 ../machinelearning-common/web-scraping-example-html-file.rmd\
 ../machinelearning-common/web-scraping-finding-elements.rmd

ggplot.md: ../data/ice-extent.csv.bz2

$(HTML): $(MDS) _bookdown.yml _output.yml ../files/style.css\
 $(PNG_FIGURES)
	Rscript -e "bookdown::render_book('index.md')"

# xopp figures in figs/*/
# figs: phony target to ensure that
# subfolder make is run just once
$(PNG_FIGURES): xopp_figs
.PHONY: xopp_figs
xopp_figs:
	$(MAKE) -C figs

clean:
	rm -fv *.nav *.out *.snm *.toc *~ *.vrb *.bbl *.dvi *.blg *.rds
