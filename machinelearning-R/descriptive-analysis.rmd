# First steps with data: descriptive analysis {#descriptive}

```{r setup, include=FALSE}
library(tidyverse)
options(tibble.width=60, tibble.print_max=20, tibble.print_min=10,
        width=79)
knitr::opts_knit$set(aliases=c(h="fig.height"))
knitr::opts_chunk$set(fig.path=".figure/descriptive/",
                      cache=TRUE, cache.path=".cache/descriptive/",
                      message=FALSE, error=FALSE, warning=FALSE, echo=TRUE)
```

When you start working with a new dataset, the first task is to do
some descriptive analysis.  This is needed for different reasons,
both to ensure that you loaded it correctly and to understand if it
contains the information you need for the analysis.


## Basic data description {#descriptive-basic}

The first steps usually involve the questions about number of rows
and columns, names of the data variables, and basic summary
information.  One can use the basic data frame syntax (see Section
\@ref(r-language-data-frames)). 

The number of rows and columns can be queried with `dim()`, `nrow()`
and `ncol()`.  Let's demonstrate this with
[titanic data](https://bitbucket.org/otoomet/lecturenotes/raw/master/data/titanic.csv.bz2):
```{r}
titanic <- read_delim("../data/titanic.csv.bz2")
dim(titanic)  # rows, columns
nrow(titanic)  # rows
ncol(titanic)  # columns
```
So the dataset contains 1309 rows and 14 columns.

For quite interactive analysis, it may be best to use `dim()` as one
quick command will give both rows and columns.  But when you need
these figures in code, `ncol()` or `nrow()` may be better.

Next, we may want to know the variable names.  This can be easily done
with `names()`:
```{r}
names(titanic)
```
But sometimes it is more useful not just to look at names but also
print a few lines of data.  Here `head()`, `tail()`, and `sample_n()`
come in handy:
```{r}
titanic %>%
   head(2)  # first few lines
titanic %>%
   tail(2)  # last few lines
titanic %>%
   sample_n(2)  # random few lines
```
The exact output depends on the type of data frame: if load or
manipulated through dplyr functions, the results are data frames of
_tibble_ flavor.  These are printed in a more compact way, but
unfortunately leaving out some of the columns and shortening the
others.  If you prefer the longer full output, you can force the
results into base-R data frames with `as.data.frame()` before printing
the lines:
```{r}
titanic %>%
   as.data.frame() %>%
   head(2)
```
Now the output includes all columns and full-length fields, but it is
very wide, it is also typically wrapped into multiple lines.
