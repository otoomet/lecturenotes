# Datasets

## Heart attack {#datasets-heart}

In
[repo](https://bitbucket.org/otoomet/lecturenotes/raw/master/datascience-intro/data/heart.csv). 

Originates from 
[kaggle](https://www.kaggle.com/datasets/rashikrahmanpritom/heart-attack-analysis-prediction-dataset/data?select=heart.csv),
claimed to be
[CC0: Public Domain](https://creativecommons.org/publicdomain/zero/1.0/)
but requires to sign in for downloading.

Variables:

* **Age**: Age of the patient
* **Sex**: Sex of the patient
* **exang**: exercise induced angina (1 = yes; 0 = no)
* **ca**: number of major vessels (0-3)
* **cp**: Chest Pain type chest pain type
    -  Value 1: typical angina
    -  Value 2: atypical angina
    -  Value 3: non-anginal pain
    -  Value 4: asymptomatic
* **trtbps**: resting blood pressure (in mm Hg)
* **chol**: cholestoral in mg/dl fetched via BMI sensor
* **fbs**: (fasting blood sugar > 120 mg/dl) (1 = true; 0 = false)
* **rest_ecg**: resting electrocardiographic results
    -  Value 0: normal
    -  Value 1: having ST-T wave abnormality (T wave inversions and/or ST elevation or depression of > 0.05 mV)
    -  Value 2: showing probable or definite left ventricular hypertrophy by Estes' criteria
* **thalach**: maximum heart rate achieved
* **target**: 0= less chance of heart attack 1= more chance of heart attack

No information is provided how the data is collected.  Not all actual
values correspond to the documentation.  The results are weird,
e.g. women and young people have higher chances for heart attack.



## Titanic {#datasets-titanic}

In
[repo](https://bitbucket.org/otoomet/lecturenotes/raw/master/data/titanic.csv.bz2). 

List of RMS _Titanic_ passengers, their name, age and some more
data, and whether they survived the shipwreck.  It was collected by
the investigation committee, and contains most of the passengers on
the boat.
The dataset is available in various sources, e.g. at
[kaggle](https://www.kaggle.com/datasets/vinicius150987/titanic3).
The variables are

* **pclass** Passenger Class (1 = 1st; 2 = 2nd; 3 = 3rd)
* **survived** Survival (0 = No; 1 = Yes)
* **name** Name
* **sex** Sex
* **age** Age
* **sibsp** Number of Siblings/Spouses Aboard
* **parch** Number of Parents/Children Aboard
* **ticket** Ticket Number
* **fare** Passenger Fare
* **cabin** Cabin
* **embarked** Port of Embarkation (C = Cherbourg; Q = Queenstown; S = Southampton)
* **boat** Lifeboat code (if survived)
* **body** Body number (if did not survive and body was recovered)
* **home.dest** The home/final destination of passenger
