# Images

Unless specified otherwise, the images are public domain (CC0).

## Images not in public domain:

* **harden-rockets-2016**: Derived from
   Keith Allison from Hanover, MD, USA, [CC BY-SA
   2.0](https://creativecommons.org/licenses/by-sa/2.0), via [Wikimedia
   Commons](https://commons.wikimedia.org/wiki/File:James_Harden_(30852408515).jpg) 

* **iris-virginica-sepal-petal.jpg**: Derived from
  Eric Hunt, [CC BY-SA
  4.0](https://creativecommons.org/licenses/by-sa/4.0), via [Wikimedia
  Commons](https://commons.wikimedia.org/wiki/File:Iris_virginica_2.jpg).

## Relationships

* **qaanaaq**: derived from
  Col. Lee-Volker Cox, Public domain, via
  [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Qaanaaq,_Greenland.jpg)


## Solutions

* **white-raven**:
  Jg26908, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0),
  via
  [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Whitecrow333.jpg). 
