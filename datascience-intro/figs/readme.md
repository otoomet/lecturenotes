# Image sources and copyrights

Images, not listed here, are made by Ott Toomet and licensed as Public
Domain
[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/?ref=chooser-v1). 
