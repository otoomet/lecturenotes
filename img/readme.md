# Image sources and copyrights

Images, not listed here, are made by Ott Toomet and licensed as Public
Domain
[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/?ref=chooser-v1). 

* **animal-game-unicorn.jpg**: by Yuemin Cao,
  [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/?ref=chooser-v1)
* **bomber-damage.png** made of [wikimedia
  file](https://commons.wikimedia.org/wiki/File:Boeing_B-17G.png) by
  Emoscopes, [CC
  BY-SA](https://creativecommons.org/licenses/by-sa/2.5)
* **fahrenheit-ratio-measure-space-com.png**: screenshot from
  [space.com](space.com), copyrighted.
* **iris-setosa.jpg**: This work has been released into the public
  domain by its author, Денис Анисимов at Russian Wikipedia. This
  applies worldwide.  Original at [wikimedia
  commons](https://commons.wikimedia.org/wiki/File:Irissetosa1.jpg).
* **lyman trestle.jpg**: public domain, from [wikimedia
  commons](https://commons.wikimedia.org/wiki/File:Lyman_viaduct_pacific_railway_1876.JPG). 
* **mh-370-ci**: Pechristener, Furfur.  Derivative work: CommonMarks, 
  [https://creativecommons.org/licenses/by-sa/3.0](CC BY-SA 3.0), 
  via [https://commons.wikimedia.org/wiki/File:Theoretical_Search_Area_MH370.v2.en.svg](Wikimedia Commons)
* **nicholas-ii-5-roubles.png**: by 
  [Unwrecker](https://ru.wikipedia.org/wiki/%D0%A3%D1%87%D0%B0%D1%81%D1%82%D0%BD%D0%B8%D0%BA:Unwrecker),
  [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en), via
  [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Russian_Empire-1899-Coin-5-Reverse.jpg)
* **m94.jpg**: [CC SA
  3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en), from [wikimedia
  commons](https://commons.wikimedia.org/wiki/File:Messier_94.jpg). 
* **presidents.png**: by Chesie Yu,
  [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1)
* **sharks-raja-ampat.jpg**: Original image by _Rolandandika_,
  [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0),
  via [Wikimedia
  Commons](https://commons.wikimedia.org/wiki/File:Sharks_in_The_Shore.jpg). 
* **spam.jpg**: by Chesie Yu,
  [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1)
