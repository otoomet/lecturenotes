\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{booktabs}
\usepackage{chessboard}
\usepackage[font={small,sl}]{caption}
\usepackage[inline]{enumitem}
\usepackage[bookmarks=TRUE,
            colorlinks,
            pdfpagemode=none,
            pdfstartview=FitH,
            citecolor=TealBlue,
            filecolor=black,
            linkcolor=blue,
            urlcolor=blue
            ]{hyperref}
\usepackage{gensymb}
\usepackage[left=25mm, textwidth=130mm]{geometry}
\usepackage{graphicx}
\usepackage{icomma}
\usepackage{kbordermatrix}
\usepackage{listings}
\usepackage{makeidx}
\usepackage{marginnote}
\usepackage{mathtools}  % aligned matrix environment, arrows w/text
\usepackage{minitoc}
\usepackage[version=4]{mhchem}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{natbib}
\usepackage{relsize}
\usepackage{savesym}
\usepackage[outercaption, wide, raggedright]{sidecap}
\usepackage{siunitx}
\usepackage{skak}
\usepackage{subcaption}
\usepackage{xcolor}
\usepackage{tikz}
\usepackage[skins,theorems,breakable]{tcolorbox}
\usepackage{tocloft}
\usepackage{wasysym}
\usepackage{wrapfig}
\usepackage{xspace}
\savesymbol{swdefault}
\let\origbar\bar \let\origdot\dot
\usepackage{allrunes}
\let\bar\origbar \let\dot\origdot
\restoresymbol{R}{swdefault}
\input{tex/isomath}

% \theoremstyle{definition}
\newtheorem*{solution}{Solution}
\newtheorem{theorem}{Theorem}
% \newtheorem{definition}{Definition}
\DeclareSIUnit{\parsec}{pc}

\tcbuselibrary{skins}
\colorlet{hiRed}{red!50}
\colorlet{paleBlue}{blue!10}
\colorlet{paleRed}{red!10}
\colorlet{palePink}{red!50!blue!20}
\colorlet{darkBlue}{black!40!blue!}

% -- Color boxes.  All have light gray background --
% * cheatsheet: dark blue
% * example: darkish green
% * exercise: dark red
% * definition:
% * proof: dark gray

% -------------------- cheatsheet (remember) --------------------
\newcommand{\listcheatsheetname}{List of Cheatsheets}
\newlistof[chapter]{cheatsheet}{chs}{\listcheatsheetname}
\colorlet{colrememberbox}{white!30!blue!70}
\colorlet{colremembertitle}{black!40!blue!}
\newenvironment{cheatsheet}[2]{
  \bigskip
  \refstepcounter{cheatsheet}
  \addcontentsline{chs}{cheatsheet}{\protect{\thecheatsheet} {#1}}
  \label{chs:#2}
  \begin{tcolorbox}[
    enhanced,  % needed for tikz commands to work
    colframe=colrememberbox,
    attach boxed title to top left,
    % Ensures proper line breaking in longer titles
    minipage boxed title,
    % (boxed title style requires an overlay)
    boxed title style={empty,size=minimal,toprule=0pt,top=4pt,left=3mm,bottom=1mm,overlay={}},
    flushleft title,
    coltitle=colremembertitle,fonttitle=\bfseries,
    before=\par\medskip\noindent,parbox=false,boxsep=0pt,left=3mm,right=3mm,top=2mm,bottom=2mm,breakable,pad at break=0mm,
    before upper=\csname @totalleftmargin\endcsname0pt, % Use instead of parbox=true. This ensures parskip is inherited by box.
    title=Cheatsheet \thecheatsheet: {#1}
    ]
  }{
  \end{tcolorbox}
}


% -------------------- definition --------------------
\newcommand{\listdefinitionname}{List of Definitions}
\newlistof[chapter]{definition}{def}{\listdefinitionname}
\colorlet{coldefinitionbox}{black!40!blue!45!red!35}
\colorlet{coldefinitiontitle}{black!40!blue!30!red!30}
\newenvironment{definition}[2]{
  \bigskip
  \refstepcounter{definition}
  \addcontentsline{def}{definition}{\protect{\thedefinition} {#1}}
  \label{def:#2}
  \begin{tcolorbox}[
    enhanced,  % needed for tikz commands to work
    colframe=coldefinitionbox,
    attach boxed title to top left,
    % Ensures proper line breaking in longer titles
    minipage boxed title,
    % (boxed title style requires an overlay)
    boxed title style={empty,size=minimal,toprule=0pt,top=4pt,left=3mm,bottom=1mm,overlay={}},
    flushleft title,
    coltitle=coldefinitiontitle,fonttitle=\bfseries,
    before=\par\medskip\noindent,parbox=false,boxsep=0pt,left=3mm,right=3mm,top=2mm,bottom=2mm,breakable,pad at break=0mm,
    before upper=\csname @totalleftmargin\endcsname0pt, % Use instead of parbox=true. This ensures parskip is inherited by box.
    title=Definition \thedefinition: {#1}
    ]
  }{
  \end{tcolorbox}
}

% -------------------- example --------------------
\newcommand{\listexamplename}{List of Examples}
\newlistof[chapter]{example}{exm}{\listexamplename}
\colorlet{colexamplebox}{green!40!black!60}
\colorlet{colexampletitle}{green!20!black!80}
\newenvironment{example}[2]{
  \vfill
  \refstepcounter{example}
  \addcontentsline{exm}{example}{\protect{\theexample} {#1}}
  \label{exm:#2}
  \begin{tcolorbox}[
    enhanced,  % needed for tikz commands to work
    colframe=colexamplebox,
    attach boxed title to top left,
    % Ensures proper line breaking in longer titles
    minipage boxed title,
    % (boxed title style requires an overlay)
    boxed title style={empty,size=minimal,toprule=0pt,top=4pt,left=3mm,bottom=1mm,overlay={}},
    flushleft title,
    coltitle=colexampletitle,fonttitle=\bfseries,
    before=\par\medskip\noindent,parbox=false,boxsep=0pt,left=3mm,right=3mm,top=2mm,bottom=2mm,breakable,pad at break=0mm,
    before upper=\csname @totalleftmargin\endcsname0pt, % Use instead of parbox=true. This ensures parskip is inherited by box.
    title=Example \theexample: {#1}
    ]
  }{
  \end{tcolorbox}
}

% -------------------- exercise --------------------
\newcommand{\listexercisename}{List of Exercises}
\newlistof[chapter]{exercise}{exc}{\listexercisename}
\colorlet{colexercisebox}{red!30!black!70}
\colorlet{colexercisetitle}{red!60!black!}
\newenvironment{exercise}[2]{
  \refstepcounter{exercise}
  \addcontentsline{exc}{exercise}{\protect{\theexercise} {#1}}
  \label{exc:#2}
  \begin{tcolorbox}[
    enhanced,  % needed for tikz commands to work
    colframe=colexercisebox,
    attach boxed title to top left,
    % Ensures proper line breaking in longer titles
    minipage boxed title,
    % (boxed title style requires an overlay)
    boxed title style={empty,size=minimal,toprule=0pt,top=4pt,left=3mm,bottom=1mm,overlay={}},
    flushleft title,
    coltitle=colexercisetitle,fonttitle=\bfseries,
    before=\par\medskip\noindent,parbox=false,boxsep=0pt,left=3mm,right=3mm,top=2mm,bottom=2mm,breakable,pad at break=0mm,
    before upper=\csname @totalleftmargin\endcsname0pt, % Use instead of parbox=true. This ensures parskip is inherited by box.
    title=Exercise \theexercise: {#1}
    ]
  }{
  \end{tcolorbox}
}

% -------------------- proof --------------------
\colorlet{colproofbox}{white!30!black!70}
\colorlet{colprooftitle}{blue!40!black!}
\newtcbtheorem{boxproof}{Proof}{
  enhanced,  % needed for tikz commands to work
  colframe=colproofbox,
  attach boxed title to top left,
  % Ensures proper line breaking in longer titles
  minipage boxed title,
  % (boxed title style requires an overlay)
  boxed title style={empty,size=minimal,toprule=0pt,top=4pt,left=3mm,bottom=1mm,overlay={}},
  flushleft title,
  coltitle=colprooftitle,fonttitle=\bfseries,
  before=\par\medskip\noindent,parbox=false,boxsep=0pt,left=3mm,right=3mm,top=2mm,bottom=2mm,breakable,pad at break=0mm,
  before upper=\csname @totalleftmargin\endcsname0pt, % Use instead of parbox=true. This ensures parskip is inherited by box.
}{prf}


% -------------------- algorithm --------------------
\lstnewenvironment{algorithm}[1][] %defines the algorithm listing environment
{   
  \lstset{ %this is the stype
    mathescape=true,
    frame=tB,
    numbers=left, 
    numberstyle=\tiny,
    basicstyle=\scriptsize, 
    keywordstyle=\color{black}\bfseries\em,
    keywords={,input, output, return, datatype, function, in, if, else, foreach, while, begin, end, let,} %add the keywords you want, or load a language as Rubens explains in his comment above.
    numbers=left,
    xleftmargin=.04\textwidth,
    #1 % this is to add specific settings to an usage of this environment (for instnce, the caption and referable label)
  }
}
{}

\newtcolorbox{fequation}[1]{
  colback=black!20!white!80,
  colframe=white!30!blue!70,
  title={#1}
}

\newenvironment{prerequisites}{
  ~\textbf{Prerequisites:}
}{
  \par\bigskip
}

%% figure at right (60\%), short caption underneath,
%% long caption at left.  In thin frame, light gray background
%% -- Arguments:
%% #1: command to include image (\includegraphcis...)
%% #2: short caption + label
%% #3: long caption
\newenvironment{rightfigure}[3]{
  \begin{figure}[ht]
    \begin{tcolorbox}[colback=black!3!white, boxrule=0.2mm, arc=0mm]
      % light-gray background
      % thin border
      % rectangular corners
    \begin{minipage}[t]{0.38\linewidth}
      \sl \small #3
    \end{minipage}
    \hspace{5mm}
    \begin{minipage}[t]{0.59\linewidth}
      \centering
      \vspace{-1ex}  % need this to force vertical alignment of figures
      #1
      \captionof{figure}{#2}
    \end{minipage}
    }{
    \end{tcolorbox}
  \end{figure}
}

% Stuff to make copying from slides easy
% \newenvironment{frame}{}{}
\newcommand{\frametitle}{}
\def\sectionautorefname{Section}
\def\subsectionautorefname{Section}
\def\paragraphautorefname{Paragraph}
\def\exampleautorefname{Example}
\def\cheatsheetautorefname{Cheatsheet}
% fullref: https://tex.stackexchange.com/questions/121865/nameref-how-to-display-section-name-and-its-number
\newcommand*{\fullref}[1]{\hyperref[{#1}]{\autoref*{#1}
    \emph{\nameref*{#1}}}, page~\pageref{#1}} % One single link
\newcommand{\alert}[1]{\textcolor{red}{#1}}
\newenvironment{columns}{}{}
\newenvironment{column}[1]{}{}
%
\newcommand{\mnote}[1]{\marginnote{\smaller\flushleft #1}}
\newcommand{\std}[1]{\small\textit{#1}}
\newcommand{\pmf}{p.m.f.\xspace}
\newcommand{\cdf}{c.d.f.\xspace}
\newcommand{\pdf}{p.d.f.\xspace}
\newcommand{\RSquared}{\ensuremath{R^{2}}\xspace}
\newcommand{\RMSE}{\ensuremath{\mathit{RMSE}}\xspace}
\newcommand*{\set}[1]{\mathcal{#1}}
\newcommand{\TBD}[1]{\par TBD: {\color{gray}{#1}\par}}
%
\numberwithin{equation}{section}
\graphicspath{{figs/}{figs/stats/}{figs/sol/}{img/}}
\makeindex
\setcounter{tocdepth}{1}

% picture ideas:
% * family has two children
% * house price
% * Titanic
% * Delivery firm has to ship 90% order in time
% * "men are taller than women"
