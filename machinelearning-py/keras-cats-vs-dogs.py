#!/usr/bin/env python3
##
import numpy as np
import pandas as pd
import tensorflow as tf
print("tensorflow:", tf.__version__)
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import os

## Data download: https://www.kaggle.com/c/dogs-vs-cats/data
## Location of images.  You should have folder 'train' in that place
## where you keep downloaded and decompressed training images.
## The file names are like 'cat.12345.jpg' and 'dog.54321.jpg'
## so category can be deduced from the file name.
imgDir = "../../teaching/data/images/cats-n-dogs"
## resize images to this dimension below
imageWidth, imageHeight = 128, 128
imageSize = (imageWidth, imageHeight)
## How many color channels
channels = 3

## Collect all training images and make a data frame
## that contains file name and the category
filenames = os.listdir(os.path.join(imgDir, "train"))  # all file names
print(len(filenames), "images found")
trainImages = pd.DataFrame({
    'filename':filenames,
    'category':pd.Series(filenames).str[:3]
    # first three letters of file name give the category
})
print("categories:\n", trainImages.category.value_counts())
# categories should be only "dog" and "cat"
print("image file dataframe sample:\n", trainImages.sample(5))

## ---------- Create model ----------
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D,MaxPooling2D,\
     Dropout, Flatten, Dense,\
     BatchNormalization

model = Sequential()
## First convolutional layer with 32 filters (kernels)
model.add(Conv2D(32,
                 kernel_size=3,
                 activation='relu',
                 input_shape=(imageWidth, imageHeight, channels)))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=2))
model.add(Dropout(0.25))
## 2nd convolutional layer
model.add(Conv2D(64,
                 kernel_size = 3,
                 activation='relu'))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=2))
model.add(Dropout(0.25))
## 3rd convolutional layer
model.add(Conv2D(128,
                 kernel_size=3,
                 activation='relu'))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=2))
model.add(Dropout(0.25))
## Flatten the image into a string of pixels
model.add(Flatten())
## Use one final dense layer
model.add(Dense(512, activation='relu'))
model.add(BatchNormalization())
model.add(Dropout(0.5))
## Output layer with 2 softmax nodes
model.add(Dense(2, activation='softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

model.summary()

## Training data generator:
train_generator = ImageDataGenerator(
    rescale=1./255,
    rotation_range=15,
    shear_range=0.1,
    zoom_range=0.2,
    horizontal_flip=True,
    width_shift_range=0.1,
    height_shift_range=0.1
).flow_from_dataframe(
    trainImages,
    os.path.join(imgDir, "train"),
    x_col='filename', y_col='category',
    class_mode='categorical',  # target is 2-D array of one-hot encoded labels
    target_size=imageSize,
    shuffle=True
)

## Model Training:
history = model.fit(
    train_generator, 
    epochs=1  # adjust for more
)

## ---------- Test data preparation ----------
testDir = os.path.join(imgDir, "test")
testImages = pd.DataFrame({
    'filename': os.listdir(testDir)
})
print(testImages.shape, "test files read from", testDir)

test_generator = ImageDataGenerator(
    rescale=1./255
    # do not randomize testing!
).flow_from_dataframe(
    testImages,
    os.path.join(imgDir, "test"),
    x_col='filename',
    class_mode = None,  # we don't want target for prediction
    target_size = imageSize,
    shuffle = False
    # do _not_ randomize the order!
    # this would clash with the file name order!
)

## Make categorical prediction:
print("Predicting all", testImages.shape[0], "test images")
phat = model.predict(test_generator)
testImages["Pr(cat)"] = phat[:,0]
testImages["Pr(dog)"] = phat[:,1]
print("Predicted array shape:", phat.shape)
print("Example predicted probabilities:")
print(phat[:5])

## Convert probabilities to categories:
testImages['category'] = np.argmax(phat, axis=-1)
## rename numeric categories to 'cat', 'dog'
label_map = {0:"cat", 1:"dog"}
testImages['category'] = testImages['category'].replace(label_map)

## display 18 random images
rows = np.random.choice(testImages.index, 18, replace=False)
print(testImages.loc[rows])
##  plot a random subset of images with labels
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
testCases = np.random.choice(testImages.index, 18)  # pick 18 random cases
# plot those on 6x3 subplot grid
ip = 1  # counts subplots
for testCase in testCases:
    im = Image.open(os.path.join(testDir, testImages.filename[testCase]))
    im = im.resize(imageSize)
    im = np.expand_dims(im, axis=0)/255
    # im = np.array(im)/255
    phat = model.predict(im)
    pred = np.argmax(phat, axis=-1)[0]
    print("sample image {} is {}".format(testImages.filename[testCase],
                                         label_map[pred]))
    ax = plt.subplot(6, 3, ip)
    ax.imshow(im[0])
    _ = ax.axis("off")
    ax.set_title(testImages.filename[testCase] + " ({})".\
                 format(label_map[pred]))
    ip += 1
plt.show()
