#!/usr/bin/env python3
## Code to be included into the rmd but not run there
## This file should work correctly to ensure we copy the correct
## examples over to the document
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from tensorflow import keras


## Decision boundary plot
def DBPlot(m, X, y, nGrid = 100, fName=None):
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.linspace(x1_min, x1_max, nGrid),
                           np.linspace(x2_min, x2_max, nGrid))
    XX = np.column_stack((xx1.ravel(), xx2.ravel()))
    phat = m.predict(XX)
    hatyy = np.argmax(phat, axis=-1).reshape(xx1.shape)
    plt.figure(figsize=(10,10))
    _ = plt.imshow(hatyy, extent=(x1_min, x1_max, x2_min, x2_max),
                   aspect="auto",
                   interpolation='none', origin='lower',
                   alpha=0.3)
    plt.scatter(X[:,0], X[:,1], c=y, s=30, edgecolors='k')
    plt.xlim(x1_min, x1_max)
    plt.ylim(x2_min, x2_max)
    if fName is not None:
        plt.savefig(fName)
    plt.show()

## Create color spiral
n = 800  # number of data points
x = np.random.normal(size=n)
y = np.random.normal(size=n)
X = np.column_stack((x,y))  # design matrix
alpha = np.arctan2(y, x)
r = np.sqrt(x**2 + y**2)
c1 = np.sin(3*alpha + 2*r)
c2 = np.cos(3*alpha + 2*r)
# cut the sum of a sin and cosine into 4 parts
category = pd.cut(c1 + c2,
                  bins=[-1.5, -1.1, -0.6, 0.6, 1.1, 1.5],
                  labels=[0, 1, 2, 3, 4]).astype(int)
nCategories = len(np.unique(category))
y = category.astype(int)

##
print("inputs: X", X.shape, "  y:", y.shape, " ", nCategories, "different categories")
print("X example:\n")
print(X[:5])
print("y example:\n")
print(y[:5])

## Plot data
_ = plt.scatter(X[:,0], X[:,1], c=y)
_ = plt.show()

## Build model
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

# sequential (not recursive) model (one input, one output)
model = Sequential()
model.add(Dense(512, activation="relu",
                input_shape=(2,)))
model.add(Dense(256, activation="relu"))
model.add(Dense(64, activation="relu"))
model.add(Dense(nCategories, activation="softmax"))

model.compile(loss='sparse_categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
print(model.summary())
## Model Training:
history = model.fit(X, y, epochs=100)

## Make categorical prediction:
print("predicting")
phat = model.predict(X)
print("Predicted array shape:", phat.shape)
print("Example:\n", phat[:5])

## Convert labels to categories:
from sklearn.metrics import confusion_matrix

yhat = np.argmax(phat, axis=-1)
print(yhat[:5])
print("confusion matrix:\n", confusion_matrix(category, yhat))
print("Accuracy (on training data):", np.mean(category == yhat))

## Plot decision boundary
## you may have to adjust the file name
DBPlot(model, X, category, fName="figs/color-spiral-keras.png")
